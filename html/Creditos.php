<!DOCTYPE html>
<html dir="en">
    <head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <!--zona de bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario-->
    <!--zona de fuente (font awesome)-->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
   
    </head>
   <body>
    <br>
     <div class="row col-md-offset-3">
    <div class="panel-heading text-center col-md-8" style="background: navy">
      <div class="form-group col-md-12">
         <h1 class="panel-title col-md-offset-1" style="color: white"><strong>Movimiento a Realizar</strong></h1>         
      </div>
</div>



<div class="panel-body col-md-8"style="border: groove">
   <div class=" col-md-1 pull-right">
<button class="fa fa-question-circle"  style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
      </div>
<form id="signupForm1" method="post" class="form-horizontal" action="" novalidate="novalidate">


<div class="form-group">
<div class="col-sm-8 ">
    <div class="form-group pull-right">
<button type="submit" class="btn btn-success"  name="signup1" value="Sign up">Transferir</button>
<a type="button" class="btn btn-primary" name="cancel" href="mov.html">Consultar</a>
    </div>

</div>
</div>
</form>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>Seleccione algun botón para realizar la operación que requiere</strong></p>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
   </div>
    
        <!-- zona de JS -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>


		

		
	

   </body>
</html>