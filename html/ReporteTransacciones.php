<?php
/*
 * script impUsuario.php
 */

//validar sesiones en caso necesario
include '../lib/fpdf/fpdf.php';
include '../php/clasesCCI.inc.php';
 #recibimos datos de formulario
/* foreach($_GET as $campo=>$valor)
 {
    $$campo=$valor;
    
 }*/
 $usuario='admin';
#buscar los datos en la base de datos
$oUsuarios=new Estudiante ;//generar una instancia  de la Clase
$resultado=$oUsuarios->listarTransacciones();

#caso especial para imprimir imagenes en pdf deste tabla BD
class VariableStream
{
	private $varname;
	private $position;

	function stream_open($path, $mode, $options, &$opened_path)
	{
		$url = parse_url($path);
		$this->varname = $url['host'];
		if(!isset($GLOBALS[$this->varname]))
		{
			trigger_error('Global variable '.$this->varname.' does not exist', E_USER_WARNING);
			return false;
		}
		$this->position = 0;
		return true;
	}

	function stream_read($count)
	{
		$ret = substr($GLOBALS[$this->varname], $this->position, $count);
		$this->position += strlen($ret);
		return $ret;
	}

	function stream_eof()
	{
		return $this->position >= strlen($GLOBALS[$this->varname]);
	}

	function stream_tell()
	{
		return $this->position;
	}

	function stream_seek($offset, $whence)
	{
		if($whence==SEEK_SET)
		{
			$this->position = $offset;
			return true;
		}
		return false;
	}
	
	function stream_stat()
	{
		return array();
	}
}

class PDF_MemImage extends FPDF
{
	function __construct($orientation='P', $unit='mm', $format='A4')
	{
		parent::__construct($orientation, $unit, $format);
		// Register var stream protocol
		stream_wrapper_register('var', 'VariableStream');
	}

	function MemImage($data, $x=null, $y=null, $w=0, $h=0, $link='')
	{
		// Display the image contained in $data
		$v = 'img'.md5($data);
		$GLOBALS[$v] = $data;
		$a = getimagesize('var://'.$v);
		if(!$a)
			$this->Error('Invalid image data');
		$type = substr(strstr($a['mime'],'/'),1);
		$this->Image('var://'.$v, $x, $y, $w, $h, $type, $link);
		unset($GLOBALS[$v]);
	}

	function GDImage($im, $x=null, $y=null, $w=0, $h=0, $link='')
	{
		// Display the GD image associated with $im
		ob_start();
		imagepng($im);
		$data = ob_get_clean();
		$this->MemImage($data, $x, $y, $w, $h, $link);
	}
} //fin variablestream
$pdf= new PDF_MemImage();
$pdf->AddPage();
//validar resultados
#encabezado
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,128);
//$pdf->SetFillColor(255,255,0);
$pdf->Image('../img/LogoTN.png',10,15,40);
$pdf->SetXY(50,22.5);
$pdf->Cell(100,10,utf8_decode('Control de Créditos de Impresiones'),0,0,'C');
$pdf->SetY(40);
$pdf->Image('../img/halcon.png',150,10,40);
$pdf->SetFont('Arial','B',14);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(190,10,'Listado de Transacciones',0,0,'C');
$pdf->Line(10,50,190,50);


if($resultado['exito']){
$pdf->SetXY(10,52);
$pdf->Cell(10,10,'#',1,0,'C');
$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,10,utf8_decode('Tipo'),1,0,'C');
$pdf->Cell(25,10,utf8_decode('Creditos'),1,0,'C');
$pdf->Cell(30,10,utf8_decode('Fecha'),1,0,'C');
$pdf->Cell(40,10,utf8_decode('Número Control'),1,0,'C');
$pdf->Cell(40,10,utf8_decode('Departamento'),1,0,'C');
$pdf->GetX()+5;
$pdf->SetY($pdf->GetY()+10);


//$pdf->Cell(30,10,'Foto',1,1,'C');
$pdf->SetFont('Arial','',8);
$pdf->SetTextColor(0,0,0);
foreach($resultado['trans'] as $llave=>$valor){
$pdf->Cell(10,10,($llave)+1,1,0,'C');
$pdf->Cell(40,10,$valor['tipo'],1,0,'C');
$pdf->Cell(25,10,utf8_decode($valor['creditos']),1,0,'C');
$pdf->Cell(30,10,utf8_decode($valor['fecha']),1,0,'C');
$pdf->Cell(40,10,utf8_decode($valor['numero']),1,0,'C');
$pdf->Cell(40,10,utf8_decode($valor['departamento']),1,0,'C');

//$pdf->Cell(30,10,$pdf->MemImage(base64_decode($valor['foto']),null,null,10),0,1,'C');
$pdf->GetX()+5;
$pdf->SetY($pdf->GetY()+10);
 
}
#pendientes del cuerpo
/*$pdf->SetFillColor(190,190,190);
$pdf->Rect(10,52,50,72,'DF');
$pdf->MemImage(base64_decode($resultado['foto_U']),14,65,36);
$pdf->SetXY(70,54);
$pdf->Cell(100,10,'Nombre:',1,2,'L',1);
$pdf->Cell(100,10,$resultado['nombre_U'],'B',2,'L');
$pdf->Cell(100,10,'Tipo:',1,2,'L',2);
$pdf->Cell(100,10,$resultado['tipo_U'],'B',0,'L');*/
} //fin de exito
else{
	$pdf->SetXY(10,60);
	$pdf->Cell(190,15,'Datos no disponibles',1,0,'C');
}
#pie de página
$pdf->SetY(-40);
$pdf->Cell(190,10,'Fecha:'.date('d-m-Y'),0,0,'R');
#


$pdf->Output('usuarios_'.$usuario.'.pdf','I');
?>