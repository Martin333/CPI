 <?php
#sesiones
    //Inicializar la sesión
    session_start();
    if (isset($_SESSION['nombre_usuario'])) {
        //asignar a variable
        $usernameSesion = $_SESSION['nombre_usuario'];
        //asegurar que no tenga "", <, > o &
        $nombre_usuario = htmlspecialchars($usernameSesion);
    }
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario -->
    
    <!--zona de font awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
</head>

   <body>
     <div class="row col-md-offset-3">
    <div class="panel-heading text-center col-md-8" style="background: navy">
      <div class="form-group col-md-12">
         <h1 class="panel-title col-md-offset-1" style="color: white"><strong>Cambiar Contraseña</strong></h1>         
      </div>
</div>



<div class="panel-body col-md-8"style="border: groove">
   <div class=" col-md-1 pull-right">
<button class="fa fa-question-circle"  style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
      </div>
   
   
<form id="editarPasswordEstudiante" method="post" name="administrador" class="form-horizontal" action="" novalidate="novalidate">
<div class="col-md-6 col-md-offset-3">
                 <div class="error alert alert-warning" role="alert" style="display:none;">
                              <strong>Datos no válidos</strong>
                 </div>
</div>


<div class="form-group">
<label class="col-sm-4 control-label" for="passwordActual">Contraseña Actual</label>
<div class="col-sm-5">
<input class="form-control" id="passwordActual" name="passwordActual" placeholder="Contraseña Actual" type="password">
</div>
</div>
<input id="nombreUsuario" type="hidden" value="<?php echo $nombre_usuario; ?>">

<div class="form-group">
<label class="col-sm-4 control-label" for="password">Nueva Contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="password" name="password" placeholder="Contraseña" type="password" readonly="true">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="confirm_password1">Confirmar Contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="confirm_password1" name="confirm_password1" placeholder="Confirmar contraseña" type="password" readonly="true">
</div>
</div>


<div class="form-group">
<div class="col-sm-9 ">
    <div class="form-group pull-right">
<button type="submit" class="btn btn-primary"  name="signup1" value="Sign up">Aceptar</button>
<a type="button" class="btn btn-danger" name="cancel" href="Estudiante.php">Cancelar</a>
    </div>

</div>
</div>
</form>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>* Contraseña Actual:</strong> Teclee la contraseña actual para habilitar la edición.</p>     
       <p><strong>* Contraseña:</strong> Llene este campo con la nueva contraseña</p>       
       <p><strong>* Confirmar contraseña:</strong> Llene este campo con la contraseña que tecleo anteriormente.</p>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
   </div>
    


<script language="javascript" src="../js/jquery-3.3.1.min.js"> </script> 
<script language="javascript" src="../js/bootstrap.min.js"> </script>
<script language="javascript" src="../js/jquery.validate.min.js"> </script>
<script language="javascript" src="../js/additional-methods.min.js"> </script>
<script language="javascript" src="../js/messages_es.min.js"> </script>
<script language="javascript" src="../js/messages_es.js"></script>

<script> //Script para buscar adeudos mas recientes
        $(document).ready(function(){
            $('#passwordActual').on('focusout ',function(){
                var nombreU=$('#nombreUsuario').val();
                var passwordU=$('#passwordActual').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarPasswordEstudiante.php',
                        dataType:'json',
                        data: {"nombreU": nombreU, "passwordU": passwordU},
    
                        success:function(resultado)
                        {
                          
                            if(resultado.exito)
                            {
                              // $('#password').val(resultado.password);
                               $('#password').prop("readonly", false);
                               //$('#confirm_password1').val(resultado.password);
                               $('#confirm_password1').prop("readonly", false);
                           
                                                  
                            }//fin del if
                            else
                            {
                              alert('La contraseña Actual es incorrecta');
                                   $('#passwordActual').val('');
                               $('#password').val('');
                               $('#confirm_password1').val('');

                               
                               $('#password').prop("readonly", true);
                               $('#confirm_password1').prop("readonly", true);

                            
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                          
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>

<script language="javascript">
    
		$.validator.setDefaults( {
			submitHandler: function () {
			 var nombreUsuario=$('#nombreUsuario').val();
     var password=$('#password').val();
     //var datosFormulario=$("#editarPasswordEstudiante").serialize();
       //datosFormulario+="&sumado=nose";
       //console.log(datosFormulario);
       $.ajax({
         type:"POST",
         url:"../php/NeditarPasswordEstudiante.php",//no autollamar
         dataType:"json",
         data:{"password": password,"nombreUsuario": nombreUsuario},
         })
       .done(function(respuesta){
         if(respuesta.exito)
         {
         alert('Cambio Exitoso');

                               $('#password').val('');
                               $('#confirm_password1').val('');
                                $('#passwordActual').val('');
                               

                               $('#password').prop("readonly", true);
                               $('#confirm_password1').prop("readonly", true);

         }
         else
         {
          
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
             
         }
          })
        
       
       .fail(function(e){
         console.log(e.responseText);
        });
         //fin de ajax
			}
		} );

		$( document ).ready( function () {
			$( "#editarPasswordEstudiante" ).validate( {
				rules: {
					firstname: "required",
					lastname: "required",
					username: {
						required: true,
						minlength: 2
					},
					password: {
						required: true,
						minlength: 8
					},
					confirm_password: {
						required: true,
						minlength: 8,
						equalTo: "#password"
					},
					
				},
				messages: {
					firstname: "Ingresa tu nombre",
					lastname: "Ingresa tu apellido",
					username: {
						required: "Please enter a username",
						minlength: "Your username must consist of at least 2 characters"
					},
					password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres"
					},
					confirm_password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres",
                        numero: "Deben ser numeros",
						equalTo: "Las contraseñas deben coincidir"
					},
				
         
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

			$( "#editarPasswordEstudiante" ).validate( {
				rules: {
					firstname1: "required",
					lastname1: "required",
               lastname2: "required",
					username1: {
						required: true,
						minlength: 2
					},
					password1: {
						required: true,
						minlength: 8
					},
					confirm_password1: {
						required: true,
						minlength: 8,
						equalTo: "#password1"
					},
					email1: {
						required: true,
						email: true
					},
                    Telefono: {
                        required: true,
                        minlength: 10,
                        pattern: "[0-9]{10}"
					},
					
				},
				messages: {
					firstname: "Ingresa tu nombre",
					lastname: "Ingresa tu apellido",
					username: {
						required: "Por favor ngresa tu nombre",
						minlength: "Al menos debe de contener 2 caracteres"
					},
					password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres"
					},
					confirm_password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres",
						equalTo: "Las contraseñas deben coincidir"
					},
                     Telefono: {
						required: "Este campo es obligatorio",
						minlength: "Debe contener al menos 10 digitos"
					},
					email: "Email no valido",
					
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-remove form-control-feedback'style='padding-top:10px'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-check form-control-feedback' style='padding-top:10px'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "fa-remove" ).removeClass( "fa-check" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "fa-check" ).removeClass( "fa-remove" );
				}
			} );
		} );
	
</script>

   </body>
</html>