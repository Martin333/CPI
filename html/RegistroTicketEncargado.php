<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    </head>
<body>
      <div class="form-container col-md-12">
       <form name="pagina" id="pag" method="post" >
         
         <br> <div class="row">
         <div class="col-md-6 col-md-offset-3">
             <div class="error alert alert-warning" role="alert" style="display:none;">
                          <strong>Datos no válidos</strong>
              </div>
           <div class="panel panel-info">
            
             
           <div class="panel-heading" style="background: navy; text-align: center">
             
          
              
             <h3 class="panel-title" style="color: white"> <strong>Registro de ticket de Compra</strong></h3>
           </div>
           <div class="panel-body">
            <!-- <div class="form-group">
                 <label class="col-md-4 ">Número de control:</label>
                 <div class="row">
                     
                     <div class="input-group col-md-4">
                     <span class="input-group-addon text-center"><i class="fa fa-user bigicon"></i></span>
                     <input type="text" placeholder="# control" class="form-control" maxlength="30" >
                  </div>
                 </div> 
             </div>-->
             <div class="form-group">
                <label class="col-md-5 ">Número de Control:</label>
                 <div class="row">
                     
                     <div class="input-group col-md-6">
                     <span class="input-group-addon text-center"><i class="fa fa-user bigicon"></i></span>
                     <input type="text" placeholder="No. Control" class="form-control" autofocus="true" maxlength="30" name="numero_control" id="numero_control" >
                  </div>
                 </div>
                 
                 <br>

                    <label class="col-md-5 ">Nombre del estudiante:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-user"></i></span>
                        <input type="text" placeholder="Nombre del estudiante" class="form-control" maxlength="30" min="0" id="nombre" name="nombre" disabled=true >
                    </div>
                    </div>
                    <br>
                

                    <label class="col-md-5 ">Créditos Disponibles:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-money"></i></span>
                        <input type="text" placeholder="Créditos disponibles" class="form-control" maxlength="30" min="0" id="creditos_disponibles" name="creditos_disponibles" disabled=true >
                    </div>
                    </div>
                    <br>
                    
                      <label class="col-md-5 ">Adeudos:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-dollar"></i></span>
                        <input type="text" placeholder="Número de Adeudos" class="form-control" maxlength="30" min="0" id="adeudos" name="adeudos" disabled=true >
                    </div>
                    </div>
                    <br><br>
                   
                 <label class="col-md-5 ">Número de ticket:</label>
                 <div class="row">
                     
                     <div class="input-group col-md-6">
                     <span class="input-group-addon text-center"><i class="fa fa-ticket"></i></span>
                     <input type="text" placeholder="No. Ticket" class="form-control" maxlength="30" name="numero_ticket" id="numero_ticket" >
                  </div>
                 </div>
                 
             </div>
             <div class="row pull-right">
               <div class="col-md-12">
                 <input class="btn btn-primary" type="submit" value="Registrar">
               </div>
             </div>
           </div>
           
           </div><!--fin panel-info-->
             </div><!--col-md-8-->
         
          </div><!-- /.row-->
      </form>  
 </div><!--/.container-->
   <!-- /ejemplo de formulario--->
 
   <!--zona de js -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"> </script> 
<script language="javascript" src="../js/bootstrap.min.js"> </script>
<script language="javascript" src="../js/jquery.validate.min.js"> </script>
<script language="javascript" src="../js/additional-methods.min.js"> </script>
<script language="javascript" src="../js/messages_es.min.js"> </script>
<script language="javascript" src="../js/messages_es.js"></script>
 
  <script> //Script para buscar creditos disponibles
        $(document).ready(function(){
            $('#numero_control').on('blur',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarCreditosActuales.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                               $('#creditos_disponibles').val(resultado.numero_creditos_actuales);
                               $('#nombre').val(resultado.nombre + ' '+ resultado.apellidos);
                            
                              
                            }//fin del if
                            else
                            {
                               $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                                $('#nombre').val('');
                               $('#adeudos').val('');
                               $('#numero_control').focus();
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
  <script> //Script para buscar adeudos mas recientes
        $(document).ready(function(){
            $('#numero_control').on('blur',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarAdeudoMasReciente.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                               $('#adeudos').val(resultado.numero_adeudos);
                               
                            }//fin del if
                            else
                            {
                              /* $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                               $('#numero_control').focus();*/
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
 
<script language="javascript"> //script para registrar ticket
  
  /*jQuery.validator.setDefaults({
   debug: true,
   success: "valid"
 });   */
   $.validator.setDefaults({
       submitHandler:function(){
         //alert("enviado!!");
         //copiar la parte de btn guardar
         //btnGuardar
     
       var datosFormulario=$("#pag").serialize();
       //datosFormulario+="&sumado=nose";
       //console.log(datosFormulario);
       $.ajax({
         type:"POST",
         url:"../php/NregistrarTicket.php",//no autollamar
         dataType:"json",
         data:datosFormulario
         })
       .done(function(respuesta){
         if(respuesta.exito)
         {
        
          alert('Registro Exitoso');
          $('#numero_control').focus();
          $('#numero_ticket').val('');
          $('#numero_ticket').focus();
//          console.log(respuesta.numero_ticket);

         }
         else
         {            
             $('#numero_ticket').val('');
             $('#numero_ticket').focus();
             alert('Número del ticket ya existe, ingresa el correcto');
             //modificar formulario
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
         }
          })
        
       
       .fail(function(e){
         console.log(e.responseText);
        });
         //fin de ajax
       },
     });
     $(document).ready(function(){
             //validacion
             $("#pag").validate({
                 rules:{
                     numero_control:{
                         required:true,
                         minlength:7
                         },
                     numero_ticket:{
                         required:true
                         //minlength:8,
                         //maxlength:16,
                         /*pattern:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_-])[A-Za-z\d$@$!%-*_?&]{8,16}$/)*/
                     }
                 },
                 messages:{
                     numero_control:{
                             required:"El Usuario es Requerido"
                            
                             },
                     numero_ticket:{
                             required:"El password es Requerido"
                             
                             }
                 },
                 errorElement:"em",errorPlacement:function(error,element){
                        error.addClass("help-block");
                        element.parents(".form-group col-md-12").addClass("has-feedback");
                        
                        if(element.prop("type")==="checkbox"){
                         error.insertAfter(element.parent("label"));
                        }
                        else{
                         error.insertAfter(element.parent());
                        }
                        if(!element.next("span")[0])
                        {
                         $("<span class=\"fa fa-remove form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
         
                     },
                     success:function(label,element){
                         if(!$(element).next("span")[0])
                        {
                         $("<span class=\"fa fa-ok form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
                     },
                     highlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-error").removeClass("has-success");
                         $(element).parents(".col-md-6").addClass("has-error").removeClass("has-success");
                         $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                         
                     },
                     unhighlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-success").removeClass("has-error");
                         $(element).parents(".col-md-6").addClass("has-success").removeClass("has-error");
                         $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                         },
                 });
             });
     
 </script>
 
   
    
  </body>
</html>
 