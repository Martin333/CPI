<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    </head>
  <body>
    
<div class="form-container col-md-12">
  <form name="pagina" id="pag" method="post" >
                 <br> <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                      <div class="error alert alert-warning" role="alert" style="display:none;">
                              <strong>Datos inválidos</strong>
                     </div>    
                      <div class="panel panel-info">
                      <div class="panel-heading" style="background: navy; text-align: center">
                        <h3 class="panel-title" style="color: white"><strong>Solicitar préstamo</strong></h3>
                      </div>
                      <div class="panel-body">
                        <div class="form-group">
                                      <label class="col-md-5 ">Número de Control:</label>
                              <div class="row">
                                  
                                  <div class="input-group col-md-6">
                                  <span class="input-group-addon text-center"><i class="fa fa-user"></i></span>
                                  <input type="text" placeholder="No. Control" class="form-control" maxlength="30" id="numero_control" name="numero_control" >
                               </div>
                                  
                          </div>
                               <br>
                                <label class="col-md-5 ">Nombre del estudiante:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-user"></i></span>
                        <input type="text" placeholder="Nombre del estudiante" class="form-control" maxlength="30" min="0" id="nombre" name="nombre" disabled=true >
                    </div>
                    </div>
                    <br>
                

                    <label class="col-md-5 ">Créditos Disponibles:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-money"></i></span>
                        <input type="text" placeholder="Créditos disponibles" class="form-control" maxlength="30" min="0" id="creditos_disponibles" name="creditos_disponibles" disabled=true >
                    </div>
                    </div>
                    <br>
                    
                      <label class="col-md-5 ">Adeudos:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-dollar"></i></span>
                        <input type="text" placeholder="Número de adeudos" class="form-control" maxlength="30" min="0" id="adeudos" name="adeudos" disabled=true >
                    </div>
                    </div>
                    <br><br>
                    
                         <label class="col-md-5 ">Créditos a prestar:</label>
                      <div class="row">
                        <div class=" input-group col-md-6">
                          <span class="input-group-addon text-center"><i class="fa fa-handshake-o"></i></span>
                        
                        <div class="dropdown">
                          
                         <select name="numero_prestamos" id="numero_prestamos" class="btn btn-default dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                          
                         <!-- <option value="1" > Uno</option>
                          <option value="2" > Dos</option>
                         <span class="caret"></span>-->
                         </select>
                                         <!--   <button class="btn btn-default dropdown-toggle" type="button" id="numero_prestamos" name="numero_prestamos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            No. préstamos
                                            <span class="caret"></span>
                                          </button>-->
                                        <!--  <ul class="dropdown-menu" aria-labelledby="numero_prestamos">
                                            <li><a href="#">Sistemas</a></li>
                                            <li><a href="#">Industrial</a></li>
                                            <li><a href="#">Electromecanica</a></li>
                                            <li><a href="#">Administrativo</a></li>
                                          </ul> -->
                        </div>
                        </div>
                        </div>
                        </div>
                       
                        
                        <div class="row pull-right">
                          <div class="col-md-10">
                            <input class="btn btn-primary" type="submit" value="Solicitar">
                          </div>
                        </div>
                      </div>
                      
                      </div><!--fin panel-info-->
                    </div><!--col-md-8-->
                    
                  </div><!-- /.row-->
                       </form>
                 </div><!--/. form-container-->
              <!-- /ejemplo de formulario--->

  <!--zona de js -->
 <script language="javascript" src="../js/jquery-3.3.1.min.js"> </script> 
<script language="javascript" src="../js/bootstrap.min.js"> </script>
<script language="javascript" src="../js/jquery.validate.min.js"> </script>
<script language="javascript" src="../js/additional-methods.min.js"> </script>
<script language="javascript" src="../js/messages_es.min.js"> </script>
<script language="javascript" src="../js/messages_es.js"></script>
 
  <script> //Script para buscar creditos disponibles
        $(document).ready(function(){
            $('#numero_control').on('blur',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarCreditosActuales.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                               $('#creditos_disponibles').val(resultado.numero_creditos_actuales);
                               $('#nombre').val(resultado.nombre + ' '+ resultado.apellidos);
                            
                              
                            }//fin del if
                            else
                            {
                               $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                               $('#nombre').val('');
                               $('#adeudos').val('');
                               $('#numero_control').focus();
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
  <script> //Script para buscar adeudos mas recientes
        $(document).ready(function(){
            $('#numero_control').on('blur',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarAdeudoMasReciente.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                               $('#adeudos').val(resultado.numero_adeudos);
                               
                            }//fin del if
                            else
                            {
                              /* $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                               $('#numero_control').focus();*/
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
 <script> //script para llenar combo box
     $(document).ready(function(){
           // $('#numero_control').on('blur',function(){
             //   var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarMaxPrestamos.php',
                        dataType:'json',
                    //    data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                             //alert(resultado.numero_maximo_prestamos);
                             var select = document.getElementById("numero_prestamos");
                              for( var $i = 1; $i <= resultado.numero_maximo_prestamos; ++$i)
                              {
                                //console.log($i);
                                  var option = document.createElement('option');
                                  option.text = option.value = $i;
                                  select.appendChild(option, 0);
                            
                               
                              }
                              
                            }//fin del if
                            else
                            {
                            
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
           //     });//fin blur
            
            });//fin del documenr
    </script>
<script language="javascript"> //Script para restarImpresiones
 
 
  /*jQuery.validator.setDefaults({
   debug: true,
   success: "valid"
 });   */
   $.validator.setDefaults({
       submitHandler:function(){
       
   
         //alert("enviado!!");
         //copiar la parte de btn guardar
         //btnGuardar
     
       var datosFormulario=$("#pag").serialize();
     
       //datosFormulario+="&sumado=nose";
       //console.log(datosFormulario);
       $.ajax({
         type:"POST",
         url:"../php/NprestarImpresiones.php",//no autollamar
         dataType:"json",
         data:datosFormulario
         })
       .done(function(respuesta){
         if(respuesta.exito)
         {
          if(respuesta.numero_adeudos<=0)
          {
          $('#numero_control').focus();
          
           $('#numero_prestamos').val("1");
           $('#numero_prestamos').focus();
          alert('Transacción exitosa');
          }
          else
          {
           $('#numero_prestamos').val("1");
          alert('Adeudo pendiente, se liquidará en la próxima compra de impresiones');
          }
        //  console.log(respuesta.nombre_usuario);

         }
         else{
          
          $('#numero_prestamos').val("1");
          //modificar formulario
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
         }
          })
        
       
       .fail(function(e){
         console.log(e.responseText);
        });
         //fin de ajax
     
    
     
      
       },//fin del submit
     });//fin del validator
   
   
    
     $(document).ready(function(){
             //validacion
             $("#pag").validate({
                 rules:{
                     numero_prestamos:{
                         required:true
                        },
                        numero_control:{
                         required:true
                        },
                    
                 },
                 messages:{
                     numero_prestamos:{
                             required:"El número de prestamos es requerido"
                            
                             },
                     numero_control:{
                         required:"El número de control es requerido"
                        },
                     
                 },
                 errorElement:"em",errorPlacement:function(error,element){
                        error.addClass("help-block");
                        element.parents(".form-group col-md-12").addClass("has-feedback");
                        
                        if(element.prop("type")==="checkbox"){
                         error.insertAfter(element.parent("label"));
                        }
                        else{
                         error.insertAfter(element.parent());
                        }
                        if(!element.next("span")[0])
                        {
                         $("<span class=\"fa fa-remove form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
         
                     },
                     success:function(label,element){
                         if(!$(element).next("span")[0])
                        {
                         $("<span class=\"fa fa-ok form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
                     },
                     highlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-error").removeClass("has-success");
                         $(element).parents(".col-md-6").addClass("has-error").removeClass("has-success");
                         $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                         
                     },
                     unhighlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-success").removeClass("has-error");
                         $(element).parents(".col-md-6").addClass("has-success").removeClass("has-error");
                         $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                         },
                 });
             });
   
     
 </script>
 
 
 
  </body>
</html>