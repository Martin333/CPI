<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario -->
    
    <!--zona de font awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
</head>

<body>
    
<div class="panel-heading text-center col-md-12" style="background: navy">
<h3 class="panel-title" style="color: white"  >Cambiar contraseña</h3>
</div>
<br>
<br>
<br>
<div class=" col-md-3 pull-right">
<button class="fa fa-question-circle"  style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
      </div>
<br>
<div class="panel-body col-md-12">
<form id="signupForm1" method="post" class="form-horizontal" action="" novalidate="novalidate">
<!--<div class="form-group">
<label class="col-sm-4 control-label" for="firstname1">Nombre</label>
<div class="col-sm-5">
<input class="form-control" id="firstname1" name="firstname1" placeholder="Nombre" type="text">
</div>
</div>-->
<!--<div class="form-group">
<label class="col-sm-4 control-label" for="lastname1">Apellido</label>
<div class="col-sm-5">
<input class="form-control" id="lastname1" name="lastname1" placeholder="Apellido" type="text">
</div>
</div>-->
<!--<div class="form-group">
<label class="col-sm-4 control-label" for="username1">Username</label>
<div class="col-sm-5">
<input class="form-control" id="username1" name="username1" placeholder="Username" type="text">
</div>
</div>-->
<!--<div class="form-group">
<label class="col-sm-4 control-label" for="email1">Email</label>
<div class="col-sm-5">
<input class="form-control" id="email1" name="email1" placeholder="Email" type="text">
</div>
</div>-->
<div class="form-group">
<label class="col-sm-4 control-label" for="passwordActual">Contraseña actual</label>
<div class="col-sm-5">
<input class="form-control" id="passwordActual" name="passwordActual" placeholder="Contraseña actual" type="password">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="password">Nueva contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="passwordNuevo" name="passwordNuevo" placeholder="Nueva contraseña" type="password">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="confirm_password">Confirmar Contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirmar contraseña" type="password">
</div>
</div>
<div class="form-group">
<div class="col-sm-5 col-sm-offset-4">
<!--<div class="checkbox">
<label>
<input id="agree1" name="agree1" value="agree" type="checkbox">He leido los terminos y condiciones
</label>
</div>-->
</div>
</div>
<div class="form-group">
<div class="col-sm-9 col-sm-offset-4">
<button type="submit" class="btn btn-primary" name="signup1" value="Sign up">Aceptar</button>
</div>
</div>
</form>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>* Contraseña Actual:</strong> Escribir la contraseña actual</p>
        
       <p><strong>* Nueva Contraseña:</strong> Escribir su nueva contraseña</p>
        
        <p><strong>* Confirmar Contraseña:</strong> Llene este campo con la nueva contraseña</p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
   </div><!--fin del modal-->
        <!-- zona de JS -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>

<!-- agregar apoyos de validación -->
<script language="javascript" src="../js/jquery.validate.min.js"></script>
<script language="javascript" src="../js/messages_es.js"></script>
<script language="javascript" src="../js/additional-methods.min.js"></script>
<script language="javascript">
    
		$.validator.setDefaults( {
			submitHandler: function () {
				alert( "submitted!" );
			}
		} );

		$( document ).ready( function () {
			$( "#signupForm1" ).validate( {
				rules: {
				passwordActual: {
						required: true,
						minlength: 8
					},
					passwordNuevo: {
						required: true,
						minlength: 8
					},
					confirm_password: {
						required: true,
						minlength: 8,
						equalTo: "#passwordNuevo"
					},
					
				},
				messages: {
					passwordActual: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres"
					},
					passwordNuevo: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres"
					},
					confirm_password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres",
						equalTo: "Las contraseñas deben coincidir"
					},
				
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

		
		} );
	
</script>
    
</body>

</html>