<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
     <!--zona de bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario -->
    
    <!--zona de font awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    
    
    
    
</head>

<body>
    <div class="container">
    <div id="mensaje"></div>
    <hr>
    <button type="button" id="muestramsg" class="btn btn-warning">Muestra Mensaje</button>
    <hr>
     <button type="button" id="lecturas" class="btn btn-warning">Leer Mensaje</button>
     <hr>
    <!-- Ejemplo de tabla-->
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>Sec.</th>
                <th>Clave</th>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Acciones</th>
                
            </tr>
        </thead>
        <tbody id="contenido">
          
        </tbody>
        <tfoot>
            <tr><td colspan="5">
                Todos los derechos reservados
            </td></tr>
        </tfoot>
    </table>
    <hr>
    <button type="button" id="llenar" class="btn btn-warning">Llenar tabla</button>
    </div>
    <!-- zona de modales -->
    <!-- edición -->
    
    <div class="modal fade" tabindex="-1" role="dialog" id="edicion">
  <div class="modal-dialog" role="document">
    <form method="post" action="" id="cambios" class="form-horizontal">
    <div class="modal-content">
     
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <div class="container">
            <div class= "row">
                <div class="col-sm-10 col-sm-offset-2">
                   <div class="form-group">
                   <label for="clave">Clave</label>
                <div class="input-group">
                   <span class="input-group-addon"> <i class="fa fa-folder-o"></i></span>
                <input type="text" class="form-control" id="clave" name = "clave" placeholder="Clave">
                </div>
            </div>
            </div>
            </div>
            <div class="form-group">
                <label for="clave">Nombre</label>
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-folder-o"></i></span>
                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
                </div>
            </div>
            <div class="form-group">
                <label for="categoria">Categoria</label>
                <div class="input-group">
                   <span class="input-group-addon"> <i class="fa fa-folder-o"></i></span>
                <input type="text" class="form-control" id="categoria" name = "categoria" placeholder="Categoria">
                </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar cambios</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    
        <!-- zona de JS -->
<script language="javascript" src="../js/jquery-3.0.0.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>

<script language="javascript">
    //validar/verificar si el documento esta cargado completamente
    //window.onLoad
    //$(function(){});
    $(document).ready(function(){
        $("#mensaje").html("Estamos usando JQuery!");
        $("#muestramsg").click(function(){
            alert($("#mensaje").text());
            });
        //para multiples eventos o eventos  propios $(selector).on("click dbclick", function(){});
       /* $("#lecturas").click(function(){
            $.ajax({
                url:"../archivos/lectura.txt", type:"GET", dataType:"text", success:function(respuesta){alert(respuesta);}, error:function(e){console.log(e.responseText);}
                });
               
            });*/
       $("#lecturas").click(function(){
        //$.ajax().done().fail();
            $.ajax({
                url:"../archivos/lectura.txt", type:"GET", dataType:"text",
                })
            .done(function(respuesta){alert(respuesta);})
            .fail(function(e){console.log(e.responseText);})
            ;
            
            });
       //proceso para llenar la tabla
       $("#llenar").on("click", function(){
        $("#contenido").html("");//limpiar tabla
        $.getJSON("../archivos/articulos.json", function(oArticulos){
            //console.log(oArticulos);
            var aArticulos=oArticulos.articulos;
            $.each(aArticulos,function(index,articulo){
                $("#contenido").append(
                "<tr id=\""+(index+1)+"\"><td>"+(index+1)+"</td><td>"+articulo.clave+"</td><td>"+articulo.nombre+"</td><td>"+articulo.categoria+"</td><td><button type=\"button\" class=\"btn btn-success btn-sm\" data-toggle=\"modal\" data-target=\"#edicion\" data-clave=\""+articulo.clave+"\" data-nombre=\""+articulo.nombre+"\" data-categoria=\""+articulo.categoria+"\"><i class=\"fa fa-pencil\" ></i></button>&nbsp;<button type=\"button\">Borrar</button><button type=\"button\">Imprimir</button></td></tr>");
                });//fin de each
            
            });//fin de .getjson
        });
        });
    //fin de document
    
    //zona de llamada a ventanas modales
    $("#edicion").on("shown.bs.modal",function(evt){
        evt.preventDefault();
        var aArticulos=$(evt.relatedTarget); //recibo los data-x
        console.log(aArticulos);
        $("#clave").val(aArticulos.data("clave"));
        $("#nombre").val(aArticulos.data("nombre"));
        $("#categoria").val(aArticulos.data("categoria"));
        
        }); //fin de show-be-modal
    
    
    //codigo para guargar
    $("#llenar").on("click",function(){
      var datosFormulario = $("#cambios").serialize();
      console.log(datosFormulario);
      $ajax({
        type: "POST",
        url: "guardar.php",
        dataType: "json",
        data: datosFormulario
        $('#cambios'.validate({
          rules:{
            clave:{requered:true,
            minlength:4,
            msxlength:4,
           },
            nombre:{datos del elementojjjjj},
            categoria:{informacion de datos},
          },
        });
      });
    
</script>

</body>

</html>