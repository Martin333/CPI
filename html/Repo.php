<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    </head>
  <body>
   
    <div class="container text-center" style="background: navy">
      <h3 style="color: white;background: navy">
        Reporte de Compras
      </h3>
    </div>
    
    <form class="container">
      <br>
      <a id="arriba"></a>
        <div class="row">
         <div class="col-md-12" >
        
        <div class="form-group">
           <div class="col-md-2">
          <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Ordenar por:
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#">N° control alumno</a></li>
                    <li><a href="#">Nombre</a></li>
                    <li><a href="#">Fecha</a></li>
                  </ul>
                </div>
        </div>
           <input type="text" class="form-container" placeholder="N° Control">
           <button class="btn btn-primary" type="button" onclick="tabla();">Buscar</button>
        <button class="fa fa-question-circle pull-right"  type="button" style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
        <a type="button" class="btn btn-success" name="cancel" href="Registro-tiket.html">Nueva Compra</a>
        </div>
        
       </div>
       </div>
     
      
      
        <div class="row">
        <div class="col-md-12 panel">
          <table class="table table-striped table-hover">
  <thead class="thead-dark" style="background: #DEDECA">

    <tr>
      <th scope="col">#</th>
      <th scope="col">N° control alumno</th>
      <th scope="col">Nombre</th>
      <th scope="col">Fecha</th>
    </tr>
  </thead>
  
  
  
 
  
  <?php 
  	include '../php/conexion.inc.php';
    
    //-------------------------------------------------
    /*
    class tabla extends Conectar
    {
      
      public function __construct()
    {
        $this->db = parent::Conectar(); //Estamos heredando, hacemos referencia a la herencia
    }//Fin de 
    
    $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT * FROM clientes";
      $qryUsuario=$this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
          
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['foto_U']=base64_encode($qryUsuario['foto_U']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    */
    }
   
    
    
    //------------------------------------------------
    
	// establecer y realizar consulta. guardamos en variable.
//	$consulta = "SELECT * FROM clientes";
	//$resultado = mysqli_query($conexion,$consulta) or die ( "Algo ha ido mal en la consulta a la base de datos");
	?>
 
 
  <tbody>
    <?php while ($row = mysqli_fetch_array($qryUsuario)) { ?>
          <tr>
            <td><?php echo $row[0]; ?></td>
            <td><?php echo $row[1]; ?></td>
            <td><?php echo $row[2]; ?></td>
            <td><?php echo $row[3]; ?></td>
          </tr>
      <?php } ?>
  </tbody>
</table>
    </div>
       
      
       
      </div>
      
      
      
       <a href="#arriba">Ir a la parte de arriba <span class="fa fa-arrow-up"></span></a>
    </form>
   


 <!--Modal-->
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>*</strong> Podras ver todas las compras que se han realizado durante el dia en la tabla.</p>
        <p><strong>* Nueva compra:</strong> Podras registrar una nueva compra presionando el boton de "nueva compra". <button class="btn btn-success" >Nueva compra</button></p>
        <p><strong>* Buscar:</strong> Podras buscar el registro de alguna compra con el numero de control del alumno que la realizo. <button class="btn btn-primary">Buscar</button></p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
    
    
     <script language="javascript" src="../js/jquery-3.3.1.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>

<!-- agregar apoyos de validación -->
<script language="javascript" src="../js/jquery.validate.min.js"></script>
<script language="javascript" src="../js/messages_es.js"></script>
<script language="javascript" src="../js/additional-methods.min.js"></script>
   
  </body>
  
</html>
 













