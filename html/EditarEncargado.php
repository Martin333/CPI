<?php
define('TIPO','mysqli');
define('SERVIDOR', 'localhost');
define('USUARIO', 'u838666589_root');
define('PASSWORD', '205am150296');
define('BASEDATOS', 'u838666589_dbcci');

$conexion = @new mysqli($server, $username, $password, $database);

if ($conexion->connect_error) //verificamos si hubo un error al conectar, recuerden que pusimos el @ para evitarlo
{
    die('Error de conexión: ' . $conexion->connect_error); //si hay un error termina la aplicación y mostramos el error
}

$sql="SELECT nombre_usuario from tbl_usuarios where tipo_usuario = 'admin'";
$result = $conexion->query($sql); //usamos la conexion para dar un resultado a la variable

if ($result->num_rows > 0) //si la variable tiene al menos 1 fila entonces seguimos con el codigo
{
    $combobit="";
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) 
    {
        $combobit .=" <option value='".$row['nombre_usuario']."'>".$row['nombre_usuario']."</option>"; //concatenamos el los options para luego ser insertado en el HTML
    }
}
else
{
    echo "No existen administradores";
}


$conexion->close(); //cerramos la conexión

?>

<!DOCTYPE html>
<html dir="en">
    <head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <!--zona de bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario-->
    <!--zona de fuente (font awesome)-->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
   
    </head>
   <body>
     <div class="row col-md-offset-3">
    <div class="panel-heading text-center col-md-8" style="background: navy">
      <div class="form-group col-md-12">
         <h1 class="panel-title col-md-offset-1" style="color: white"><strong>Editar Datos</strong></h1>         
      </div>
</div>
<div class="panel-body col-md-8"style="border: groove">
   <div class=" col-md-1 pull-right">
<button class="fa fa-question-circle"  style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
      </div>
<form id="editarEncargado" method="post" class="form-horizontal" action="" novalidate="novalidate">
  <div class="error alert alert-warning" role="alert" style="display:none;">
                          <strong>Datos no válidos</strong>
              </div>
  <div class="form-group">
<label class="col-sm-4 control-label" for="nombre_usuario">Nombre del encargado</label>
<div class="col-sm-5">
<input class="form-control" id="nombre_usuario" name="nombre_usuario" placeholder="Nombre del Encargado" type="text">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="firstname1">Nombre</label>
<div class="col-sm-5">
<input class="form-control" id="firstname1" name="firstname1" placeholder="Nombre" type="text">
</div>

</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="lastname1">Apellidos</label>
<div class="col-sm-5">
<input class="form-control" id="lastname1" name="lastname1" placeholder="Apellido" type="text">
</div>
</div>

<div class="form-group">
<label class="col-sm-4 control-label" for="departamento">Departamento:</label>
<div class="col-sm-5">
<div class="dropdown">
               
                  <select class="btn btn-default dropdown-toggle" name="departamento" id="departamento" aria-labelledby="dropdownMenu1">
                    <option><a href="#">Sistemas</a></option>
                    <option><a href="#">Industrial</a></option>
                    <option><a href="#">Electromecanica</a></option>
                    <option><a href="#">Administrativo</a></option>
                  </select>
                </div>
</div>
</div>

<div class="form-group">
<label class="col-sm-4 control-label" for="email1">Email</label>
<div class="col-sm-5">
<input class="form-control" id="email1" name="email1" placeholder="Email" type="text">
</div>
</div>

<div class="form-group">
<label class="col-sm-4 control-label" for="Telefono">Telefono:</label>
<div class="col-sm-5">
<input class="form-control" id="Telefono" name="Telefono" placeholder="######" type="Text">
</div>
</div>

<div class="form-group">
<div class="col-sm-9 ">
    <div class="form-group pull-right">
<button type="submit" class="btn btn-primary"  name="signup1" value="Sign up">Aceptar</button>
<a type="button" class="btn btn-danger" name="cancel" href="Repo-mov.html">Cancelar</a>
    </div>

</div>
</div>
</form>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>* Nombre y Apellido:</strong> Llene estos campos con sus nombre y sus aplellidos.</p>
        
       <p><strong>* Email:</strong> Llene este campo con su dirrecion de correo electronico.</p>
        
       <p><strong>* Confirmar contraseña:</strong> Llene este campo con la contraseña que tecleo anteriormente.</p>
        
        <p><strong>* Departamento:</strong> Seleccione el departamento que se quiere asignar.</p>
        
        <p><strong>* Telefono:</strong> Llene este campo con los digitos de su numero telefonico.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
   </div>
    

        <!-- zona de JS -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>

<!-- agregar apoyos de validación -->
<script language="javascript" src="../js/jquery.validate.min.js"></script>
<script language="javascript" src="../js/messages_es.js"></script>
<script language="javascript" src="../js/additional-methods.min.js"></script>
<script language="javascript">
    
		$.validator.setDefaults( {
			submitHandler: function () {
				alert( "Enviado!" );
			}
		} );

		$( document ).ready( function () {
			$( "#editarEncargado" ).validate( {
				rules: {
           nombre_usuario:{
            required: true,
            minlength: 6
          },
					firstname1: "required",
					lastname1: "required",
               lastname2: "required",
					username1: {
						required: true,
						minlength: 2
					},
					password1: {
						required: true,
						minlength: 6
					},
					confirm_password1: {
						required: true,
						minlength: 6,
						equalTo: "#password1"
					},
					email1: {
						required: true,
						email: true
					},
                    Telefono: {
						required: true,
                        minlength: 10,
                        pattern: "[0-9]{10}"
					},
					
				},
				messages: {
          nombre_usuario:{
            required: "Este campo es obligatorio",
            minlength: "Debe contener al menos 6 caracteres"
          },
					firstname: "Ingresa tu nombre",
					lastname: "Ingresa tu apellido",
					username: {
						required: "Por favor ngresa tu nombre",
						minlength: "Al menos debe de contener 2 caracteres"
					},
					password1: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 6 caracteres"
					},
					confirm_password1: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 6 caracteres",
						equalTo: "Las contraseñas no coinciden"
					},
                     Telefono: {
						required: "Este campo es obligatorio",
						minlength: "Debe contener al menos 10 digitos",
            pattern: "Formato Invalido"
					},
					email: "Email no valido",
					
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-remove form-control-feedback'style='padding-top:10px'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-check form-control-feedback' style='padding-top:10px'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "fa-remove" ).removeClass( "fa-check" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "fa-check" ).removeClass( "fa-remove" );
				}
			} );
		} );
</script>
 <script> //Script para buscar encargado
        $(document).ready(function(){
           $('#nombre_usuario').on('blur',function(){
                var nombreUsuario=$('#nombre_usuario').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarDatosEncargado.php',
                        dataType:'json',
                        data:'nombreUsuario='+nombreUsuario, //asociado y valor
                        success:function(resultado)
                        {
                          if(resultado.exito){
                           
                               $('#firstname1').val(resultado.nombre);
                               $('#lastname1').val(resultado.apellidos);
                               $('#email1').val(resultado.correo);
                               $('#Telefono').val(resultado.telefono);
                               $('#departamento').val(resultado.departamento);
                          }//fin del if
                        },
                      
                    });//fin ajax
                
               });//fin blur
            
            });//fin del documenr
    </script>
 <script language="javascript">
    
		$.validator.setDefaults( {
			submitHandler: function () {
			 var datosFormulario=$("#editarEncargado").serialize();
       //datosFormulario+="&sumado=nose";
       //console.log(datosFormulario);
       $.ajax({
         type:"POST",
         url:"../php/NeditarEncargado.php",//no autollamar
         dataType:"json",
         data:datosFormulario
         })
       .done(function(respuesta){
         if(respuesta.exito)
         {
         alert('Cambio Exitoso');
                $('#firstname1').val(resultado.nombre);
                               $('#lastname1').val("");
                               $('#email1').val("");
                               $('#Telefono').val("");
                               $('#departamento').val("");
         }
         else
         {
          
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
             
         }
       })
         .fail(function(e){
         console.log(e.responseText);
        });
          },
			});
    </script>
   </body>
</html>