<!DOCTYPE html>
<html dir="en">
    <head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <!--zona de bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario-->
    <!--zona de fuente (font awesome)-->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
   
    </head>
   <body>
     <div class="row col-md-offset-3">
    <div class="panel-heading text-center col-md-8" style="background: navy">
      <div class="form-group col-md-12">
         <h1 class="panel-title col-md-offset-1" style="color: white"><strong>Nuevo Usuario</strong></h1>         
      </div>
</div>

<div class="panel-body col-md-8"style="border: groove">
   <div class=" col-md-1 pull-right">
<button class="fa fa-question-circle"  style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
      </div>
<form id="signupForm1" method="post" class="form-horizontal col-md-12" action="" novalidate="novalidate">
<div class="form-group">
<label class="col-sm-4 control-label" for="firstname1">Nombre</label>
<div class="col-sm-5">
<input class="form-control" id="firstname1" name="firstname1" placeholder="Nombre" type="text">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="lastname1">Apellido Paterno</label>
<div class="col-sm-5">
<input class="form-control" id="lastname1" name="lastname1" placeholder="Apellido" type="text">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="lastname1">Apellido Materno</label>
<div class="col-sm-5">
<input class="form-control" id="lastname2" name="lastname2" placeholder="Apellido" type="text">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="email1">Email</label>
<div class="col-sm-5">
<input class="form-control" id="email1" name="email1" placeholder="Email" type="text">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="password1">Contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="password1" name="password1" placeholder="Contraseña" type="password">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="confirm_password1">Confirmar Contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="confirm_password1" name="confirm_password1" placeholder="Confirmar contraseña" type="password">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="confirm_password1">Telefono:</label>
<div class="col-sm-5">
<input class="form-control" id="Telefono" name="Telefono" placeholder="######" type="Text">
</div>
</div>

<div class="form-group">
<div class="col-sm-9 ">
    <div class="form-group pull-right">
<button type="submit" class="btn btn-primary"  name="signup1" value="Sign up">Aceptar</button>
<a type="button" class="btn btn-danger" name="cancel" href="Repo-mov.html">Cancelar</a>
    </div>

</div>
</div>
</form>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>* Nombre y Apellido:</strong> Llene estos campos con sus nombre y sus aplellidos.</p>
        
       <p><strong>* Email:</strong> Llene este campo con su dirrecion de correo electronico.</p>
        
       <p><strong>* Confirmar contraseña:</strong> Llene este campo con la contraseña que tecleo anteriormente.</p>
        
        <p><strong>* Telefono:</strong> Llene este campo con los digitos de su numero telefonico.</p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
   </div>
    

        <!-- zona de JS -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>

<!-- agregar apoyos de validación -->
<script language="javascript" src="../js/jquery.validate.min.js"></script>
<script language="javascript" src="../js/messages_es.js"></script>
<script language="javascript" src="../js/additional-methods.min.js"></script>
<script language="javascript">
    
		$.validator.setDefaults( {
			submitHandler: function () {
				alert( "Enviado!" );
			}
		} );

		$( document ).ready( function () {
			$( "#signupForm1" ).validate( {
				rules: {
					firstname1: "required",
					lastname1: "required",
               lastname2: "required",
					username1: {
						required: true,
						minlength: 2
					},
					password1: {
						required: true,
						minlength: 8
					},
					confirm_password1: {
						required: true,
						minlength: 8,
						equalTo: "#password1"
					},
					email1: {
						required: true,
						email: true
					},
                    Telefono: {
						required: true,
                        minlength: 10,
                        pattern: "[0-9]{10}"
					},
					
				},
				messages: {
					firstname: "Ingresa tu nombre",
					lastname: "Ingresa tu apellido",
					username: {
						required: "Por favor ngresa tu nombre",
						minlength: "Al menos debe de contener 2 caracteres"
					},
					password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres"
					},
					confirm_password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres",
						equalTo: "Las contraseñas deben coincidir"
					},
                     Telefono: {
						required: "Este campo es obligatorio",
						minlength: "Debe contener al menos 10 digitos"
					},
					email: "Email no valido",
					
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-remove form-control-feedback'style='padding-top:10px'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-check form-control-feedback' style='padding-top:10px'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "fa-remove" ).removeClass( "fa-check" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "fa-check" ).removeClass( "fa-remove" );
				}
			} );
		} );
	
</script>
   </body>
</html>