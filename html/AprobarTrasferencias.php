<?php
#sesiones
session_start();
?>
<?php
$server     = 'localhost'; //servidor
$username   = 'u838666589_root'; //usuario de la base de datos
$password   = '205am150296'; //password del usuario de la base de datos
$database   = 'u838666589_dbcci'; //nombre de la base de datos

$conexion = @new mysqli($server, $username, $password, $database);

if ($conexion->connect_error) //verificamos si hubo un error al conectar, recuerden que pusimos el @ para evitarlo
{
    die('Error de conexión: ' . $conexion->connect_error); //si hay un error termina la aplicación y mostramos el error
}
//$nControl = $_SESSION['numero_control'];
$numero_control =  $_SESSION['numero_control'];
$sql="SELECT numero_control FROM tbl_transacciones WHERE (numero_control_receptor ='$numero_control' and tipo_transaccion='solicitud_trasferencia')";
$result = $conexion->query($sql); //usamos la conexion para dar un resultado a la variable

if ($result->num_rows > 0) //si la variable tiene al menos 1 fila entonces seguimos con el codigo
{
    $combobit="";
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) 
    {
        $combobit .=" <option value='".$row['numero_control']."'>".$row['numero_control']."</option>"; //concatenamos el los options para luego ser insertado en el HTML
    }
}
else
{
    echo "No hubo resultados";
    //($_SESSION['estado_trasferencia']="sin_solicitudes");
}
$conexion->close(); //cerramos la conexión
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/awesome/css/font-awesome.min.css">
 
  <title>Movimientos</title>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario -->
    <link rel="stylesheet" href="../js/bootstrap.min.js">
    
    <!--zona de font awesmoe -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    
</head>

<body>
    
    <div class="form-container col-md-12">
         <form name="pag" id="pag" method="post" >
           
           <br> <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="error alert alert-warning" role="alert" style="display:none;">
                            <strong>Datos no válidos</strong>
                </div>
             <div class="panel panel-info">
              
               
             <div class="panel-heading" style="background: navy; text-align: center">
               
            
                
               <h3 class="panel-title" style="color: white"><strong>Solicitudes de trasferencias</strong></h3>
             </div>
             <div class="panel-body">
    <div class="form-group" >
          <form class="form-horizontal">
  <fieldset>
  <label class="col-md-5 ">Número de control:</label>
<div class="input-group-btn">
  
            <div class="dropdown">
              
                    <select class="btn btn-default dropdown-toggle" name="numero_control" id="numero_control"  aria-labelledby="dropdownMenu1">
                         <option value="" ></option>
                           <?php echo $combobit; ?>
                    </select> 
            </div>             
  </div>
<br>
  
    <label class="col-md-5 ">Nombre del estudiante:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-user"></i></span>
                        <input type="text" placeholder="Nombre del estudiante" class="form-control" id="nombre" name="nombre" readonly=true >
                    </div>
                    </div>
                    <br>
                

                    <label class="col-md-5 ">Créditos solicitados:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-money"></i></span>
                        <input type="text" placeholder="Créditos solicitados" class="form-control" maxlength="30"  min="0" id="cantidad" name="cantidad" readonly=true >
                    </div>
                    </div>
                    <br>
                    
                     

  
  <!-- Button (Double) -->
  <div class="form-group" >
    <label class="col-md-4 control-label" for="confirmar" ></label>
    <div class="col-md-8">
      <button id="confirmar" name="confirmar" type="submit"class="btn btn-success">Aprobar</button>
      <button id="rechazar" name="rechazar" type="reset" class="btn btn-danger">Rechazar</button>
    </div>
  </div>
  
  </fieldset>
  </form>
  
        </div>
              </div>
              
            </div>
          </form>
          </div>
        </div>
       </div>
    
  
    
             
 
   
    
   <!--zona de js -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"> </script> 
<script language="javascript" src="../js/bootstrap.min.js"> </script>
<script language="javascript" src="../js/jquery.validate.min.js"> </script>
<script language="javascript" src="../js/additional-methods.min.js"> </script>
<script language="javascript" src="../js/messages_es.min.js"> </script>
<script language="javascript" src="../js/messages_es.js"></script>

 <script> //Script para buscar Estudiantes ya registrados
        $(document).ready(function(){
          
            $('#numero_control').on('change',function(){
               var numeroControl=$('#numero_control').val();
                //var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarCreditosTrasferencia.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                            
                            /* if (resultado.numero_control == $('#sNumeroControl'))
                            //  {
                                  $('#cantidad').prop("readonly", true);
                                  $('#accion').prop("disabled", true);
                                  $('#numero_control').val("");                             
                                  alert('No te puedes trasferir a ti mismo!!, escribe otro número de control');
                                    $('.error').slideDown('slow');
                                 setTimeout(function(){
                                  $('.error').slideUp('slow');
                                   }, 3000);
                                  $('#numero_control').select();
                           //   }
                           //   else
                           //   {*/
                                //  $('#cantidad').prop("readonly", false);
                                  $('#cantidad').val(resultado.creditos_transaccion);
                               //   $('#accion').prop("disabled", false);
                           //       $('#cantidad').focus();
                           //   }
                          
                            }//fin del if
                            else
                            {
                                 alert('Usuario no existe!!');
                                $('.error').slideDown('slow');
                             setTimeout(function(){
                              $('.error').slideUp('slow');
                               }, 3000);
                              $('#cantidad').prop("readonly", true);
                             // $('#accion').prop("disabled", true);
                             // $('#numero_control').val("");
                              
                             
                               
                            //  $('#numero_control').select();
                              
                              
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
 <script> //Script para buscar creditos disponibles
        $(document).ready(function(){
            $('#numero_control').on('change',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarCreditosActuales.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                            //   $('#creditos_disponibles').val(resultado.numero_creditos_actuales);
                               $('#nombre').val(resultado.nombre + ' '+ resultado.apellidos);
                              
                            
                              
                            }//fin del if
                            else
                            {
                                 $('#nombre').prop('readonly',true);
                               $('#numero_control').val('');
                             //  $('#creditos_disponibles').val('');
                              // $('#impresiones_resta').val('');
                                $('#nombre').val('');
                             //  $('#adeudos').val('');
                               $('#numero_control').focus();
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
  
  <script language="javascript"> //Script para Aprobar Trasferencia
   $.validator.setDefaults({
       submitHandler:function(){
       var datosFormulario=$("#pag").serialize();
       $.ajax({
         type:"POST",
         url:"../php/NaprobarTrasferencia.php",//no autollamar
         dataType:"json",
         data:datosFormulario
       })
       .done(function(respuesta){
         if(respuesta.exito)
         {
            alert('Trasferencia Exitosa');
          $('#numero_control').focus();
          $('#cantidad').val('');
          $('#cantidad').focus();
         }
         else{
             alert('Ha surgido un error inesperado')
             $('#numero_control').focus();
          $('#cantidad').val('');
          $('#cantidad').focus();
          //modificar formulario
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
         }
          })
       .fail(function(e){
         console.log(e.responseText);
        });
         //fin de ajax
       },//fin del submit
     });//fin del validator
</script>

     <script>
     $(document).ready(function(){
             //validacion
             $("#pag").validate({
                 rules:{
                     numero_control:{
                         required:true,
                         minlength:7
                         },
                     nombre:{
                         required:true,
                       //  minlength:1,
                         //maxlength:16,
                         /*pattern:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_-])[A-Za-z\d$@$!%-*_?&]{8,16}$/)*/
                     },
                     accion:{
                         required:true
                        // minlength:0,
                         //maxlength:16,
                     }
                 },
                 messages:{
                     numero_control:{
                             required:"El número de control es requerido"
                            
                             },
                     nombre:{
                             required:"La cantidad es Requerida",
                           //  minLength:"La cantidad tiene que ser mayor a 0 ",
                             },
                    accion:{
                             required:"Seleccione una opción"
                             },
                 },
                 errorElement:"em",errorPlacement:function(error,element){
                        error.addClass("help-block");
                        element.parents(".form-group col-md-12").addClass("has-feedback");
                        
                        if(element.prop("type")==="checkbox"){
                         error.insertAfter(element.parent("label"));
                        }
                        else{
                         error.insertAfter(element.parent());
                        }
                        if(!element.next("span")[0])
                        {
                         $("<span class=\"fa fa-remove form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
         
                     },
                     success:function(label,element){
                         if(!$(element).next("span")[0])
                        {
                         $("<span class=\"fa fa-ok form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
                     },
                     highlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-error").removeClass("has-success");
                         $(element).parents(".col-md-6").addClass("has-error").removeClass("has-success");
                         $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                         
                     },
                     unhighlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-success").removeClass("has-error");
                         $(element).parents(".col-md-6").addClass("has-success").removeClass("has-error");
                         $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                         },
                 });
             });
     
 </script>
 
  
</body>

</html>
