<?php
#sesiones
session_start();
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/awesome/css/font-awesome.min.css">
 
  <title>Movimientos</title>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario -->
    <link rel="stylesheet" href="../js/bootstrap.min.js">
    
    <!--zona de font awesmoe -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    
</head>

<body>
    
    <div class="form-container col-md-12">
         <form name="pagina" id="pag" method="post" >
           
           <br> <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="error alert alert-warning" role="alert" style="display:none;">
                            <strong>Datos no válidos</strong>
                </div>
             <div class="panel panel-info">
              
               
             <div class="panel-heading" style="background: navy; text-align: center">
               
            
                
               <h3 class="panel-title" style="color: white"><strong>Trasferencias de créditos</strong></h3>
             </div>
             <div class="panel-body">
    <div class="form-group" >
          <form class="form-horizontal">
  <fieldset>
  
  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-5 control-label" for="numero_control">Número de control: </label>
    <div class="row">
    <div class=" input-group col-md-6">
    <span class="input-group-addon text-center"><i class="fa fa-user"></i></span>
    <input id="numero_control" name="numero_control" placeholder="No. Control a trasferir o solicitar" class="form-control input-md" required=""  maxlength="30" type="text">
    </div>
     <label class="col-md-3 control-label" for="numero_control"></label>
    <div class=" input-group col-md-9">
    <span class="help-block">¿A quién le enviarás créditos? o ¿A quién le solicitarás una trasferencia?</span>
    </div>
      <br>
    </div>
  </div>
  
    <label class="col-md-5 ">Nombre del estudiante:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-user"></i></span>
                        <input type="text" placeholder="Nombre del estudiante" class="form-control" maxlength="30" min="0" id="nombre" name="nombre" disabled=true >
                    </div>
                    </div>
                    <br>
                

                    <label class="col-md-5 ">Créditos Disponibles:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-money"></i></span>
                        <input type="text" placeholder="Créditos disponibles" class="form-control" maxlength="30"  min="0" id="creditos_disponibles" name="creditos_disponibles" readonly=true >
                    </div>
                    </div>
                    <br>
                    
                      <label class="col-md-5 ">Adeudos:</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                        <span class="input-group-addon text-center"><i class="fa fa-dollar"></i></span>
                        <input type="text" placeholder="Número de Adeudos" class="form-control" maxlength="30" min="0" id="adeudos" name="adeudos" disabled=true >
                    </div>
                    </div>
                    <br><br>

  
  <!-- Button Drop Down -->
  <div class="form-group">
     
    <label class="col-md-5 control-label" for="cantidad">Cantidad de créditos</label>
    
      <div class="input-group col-md-6">
        <span class="input-group-addon text-center"><i class="fa fa-hashtag"></i></span>
        <input type="number" id="cantidad" name="cantidad" class="form-control" placeholder="Cantidad"  min="1" readonly="true">
        <div class="input-group-btn">
            <div class="dropdown">
                 
                    <select class="btn btn-default dropdown-toggle" name="accion" id="accion" disabled="true"  aria-labelledby="dropdownMenu1">
                      <option value="trasferir_creditos"><a href="#">Trasferir créditos</a></option>
                      <option value="solicitar_trasferencia"><a href="#">Solicitar trasferencia</a></option>
                      
                    </select>
                     
            
                  </div>      
          
        </div>
      </div>
       <br>
    
  </div>

  
  <!-- Button (Double) -->
  <div class="form-group" >
    <label class="col-md-4 control-label" for="confirmar" ></label>
    <div class="col-md-8">
      <button id="confirmar" name="confirmar" class="btn btn-success">Confirmar</button>
      <button id="cancelar" name="cancelar" class="btn btn-danger">Cancelar</button>
    </div>
  </div>
  
  </fieldset>
  </form>
  
        </div>
              </div>
              
            </div>
          </form>
          </div>
        </div>
       </div>
    
  
    
             
 
   
    
   <!--zona de js -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"> </script> 
<script language="javascript" src="../js/bootstrap.min.js"> </script>
<script language="javascript" src="../js/jquery.validate.min.js"> </script>
<script language="javascript" src="../js/additional-methods.min.js"> </script>
<script language="javascript" src="../js/messages_es.min.js"> </script>
<script language="javascript" src="../js/messages_es.js"></script>

 <script> //Script para buscar Estudiantes ya registrados
        $(document).ready(function(){
            $('#numero_control').on('focusout',function(){
              
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarEstudiante.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                            
                            /* if (resultado.numero_control == $('#sNumeroControl'))
                            //  {
                                  $('#cantidad').prop("readonly", true);
                                  $('#accion').prop("disabled", true);
                                  $('#numero_control').val("");                             
                                  alert('No te puedes trasferir a ti mismo!!, escribe otro número de control');
                                    $('.error').slideDown('slow');
                                 setTimeout(function(){
                                  $('.error').slideUp('slow');
                                   }, 3000);
                                  $('#numero_control').select();
                           //   }
                           //   else
                           //   {*/
                                  $('#cantidad').prop("readonly", false);
                                  $('#accion').prop("disabled", false);
                                  $('#cantidad').focus();
                           //   }
                          
                            }//fin del if
                            else
                            {
                                 alert('Usuario no existe!!');
                                $('.error').slideDown('slow');
                             setTimeout(function(){
                              $('.error').slideUp('slow');
                               }, 3000);
                              $('#cantidad').prop("readonly", true);
                              $('#accion').prop("disabled", true);
                             // $('#numero_control').val("");
                              
                             
                               
                            //  $('#numero_control').select();
                              
                              
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
 <script> //Script para buscar creditos disponibles
        $(document).ready(function(){
            $('#numero_control').on('blur',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarCreditosActuales.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                               $('#creditos_disponibles').val(resultado.numero_creditos_actuales);
                               $('#nombre').val(resultado.nombre + ' '+ resultado.apellidos);
                            
                              
                            }//fin del if
                            else
                            {
                               $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                                $('#nombre').val('');
                               $('#adeudos').val('');
                               $('#numero_control').focus();
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
  <script> //Script para buscar adeudos mas recientes
        $(document).ready(function(){
            $('#numero_control').on('blur',function(){
                var numeroControl=$('#numero_control').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NbuscarAdeudoMasReciente.php',
                        dataType:'json',
                        data:'numero_control='+numeroControl, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                               $('#adeudos').val(resultado.numero_adeudos);
                               
                            }//fin del if
                            else
                            {
                              /* $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                               $('#numero_control').focus();*/
                              
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
                });//fin blur
            
            });//fin del documenr
    </script>
  <script> //Script para validar la cantidad
        $(document).ready(function(){
            $('#cantidad').on('blur',function(){
              var cod = document.getElementById("accion").value;
              if (cod=="trasferir_creditos")//onblur con trasferir creditos
              {
               if($('#cantidad').val() > <?php echo $_SESSION['creditos_actuales'];?>)
               {
                        alert('No cuentas con suficientes créditos, sugeriremos otra cantidad');
                    $('#cantidad').val(<?php echo $_SESSION['creditos_actuales'];?>);
               //modificar formulario
                    $('.error').slideDown('slow');
                    setTimeout(function(){
                         $('.error').slideUp('slow');
                     }, 3000);
               }
              }//fin trasferir créditos
              else
              {
              var cod = document.getElementById("accion").value;
              if (cod== "solicitar_trasferencia")
              {
               // console.log($('#cantidad').val());
               // console.log($('#creditos_disponibles').val());
                if(parseInt( $('#cantidad').val())> parseInt($('#creditos_disponibles').val()  ))
               {
                        alert('El usuario a solicitar trasferencia  no cuenta con suficientes créditos, sugeriremos otra cantidad');
                    $('#cantidad').val($('#creditos_disponibles').val() );
               //modificar formulario
                    $('.error').slideDown('slow');
                    setTimeout(function(){
                         $('.error').slideUp('slow');
                     }, 3000);
               }
               }
              }
    
                
                });//fin blur
            
            });//fin del documenr
    </script>
  
  <script language="javascript"> //script para trasferir créditos
    $.validator.setDefaults({
       submitHandler:function(){
 var cod = document.getElementById("accion").value;
if (cod=="trasferir_creditos")
  {   
    if($('#cantidad').val() <= <?php echo $_SESSION['creditos_actuales'];?>)
    {
 
       var datosFormulario=$("#pag").serialize();
       $.ajax({
         type:"POST",
         url:"../php/NregistrarTrasferencia.php",//no autollamar
         dataType:"json",
         data:datosFormulario
         })
       .done(function(respuesta){
         if(respuesta.exito)
         {
          alert('Trasferencia Exitosa');
          $('#numero_control').focus();
          $('#cantidad').val('');
          $('#accion').val('trasferir_creditos');
          $('#cantidad').focus();
//          console.log(respuesta.numero_ticket);

         }
         else
         {
                   //vacia password
            $('#numero_control').focus();
          $('#cantidad').val('');
          $('#accion').val('trasferir_creditos');
          $('#cantidad').focus();
             //modificar formulario
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
         }
          })
       .fail(function(e){
         console.log(e.responseText);
        });
         //fin de ajax
      
    }//fin del if validacion de cantidad menor a credtos actuales
    else
    {
      alert('No cuentas con suficientes créditos, sugeriremos otra cantidad');
      $('#cantidad').val(<?php echo $_SESSION['creditos_actuales'];?>);
        //modificar formulario
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
    }
  }//fin del if inicial
  
             
  else   //evaluar cuando se solicia una trasferencia
  {
     var cod = document.getElementById("accion").value;
    if (cod== "solicitar_trasferencia")
    {
            if(parseInt($('#cantidad').val()) <= parseInt( $('#creditos_disponibles').val()) )
            {
             
               var datosFormulario=$("#pag").serialize();
               $.ajax({
                 type:"POST",
                 url:"../php/NsolicitarTrasferencia.php",//no autollamar
                 dataType:"json",
                 data:datosFormulario
                 })
               .done(function(respuesta){
                 if(respuesta.exito)
                 {
                
                  alert('Solicitud Exitosa');
                  $('#numero_control').focus();
                  $('#cantidad').val('');
                  $('#accion').val('trasferir_creditos');
                  $('#cantidad').focus();
        //          console.log(respuesta.numero_ticket);
        
                 }
                 else
                 {
                           //vacia password
                    $('#numero_control').focus();
                  $('#cantidad').val('');
                  $('#accion').val('trasferir_creditos');
                  $('#cantidad').focus();
                     //modificar formulario
                     $('.error').slideDown('slow');
                     setTimeout(function(){
                          $('.error').slideUp('slow');
                     }, 3000);
                 }
                  })
                
               
               .fail(function(e){
                 console.log(e.responseText);
                });
                 //fin de ajax
            
            }//fin del if validacion de cantidad menor a credtos disponibles
            else
            {
              alert('El usuario a solicitar trasferecia no cuenta con suficientes créditos, sugeriremos otra cantidad');
              $('#cantidad').val($('#creditos_disponibles').val());
                //modificar formulario
                     $('.error').slideDown('slow');
                     setTimeout(function(){
                          $('.error').slideUp('slow');
                     }, 3000);
            }
    }
    
  }
   },
     });
   
   
     $(document).ready(function(){
             //validacion
             $("#pag").validate({
                 rules:{
                     numero_control:{
                         required:true,
                         minlength:7
                         },
                     cantidad:{
                         required:true,
                         minlength:1,
                         //maxlength:16,
                         /*pattern:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_-])[A-Za-z\d$@$!%-*_?&]{8,16}$/)*/
                     },
                     accion:{
                         required:true
                        // minlength:0,
                         //maxlength:16,
                     }
                 },
                 messages:{
                     numero_control:{
                             required:"El número de control es requerido"
                            
                             },
                     cantidad:{
                             required:"La cantidad es Requerida",
                             minLength:"La cantidad tiene que ser mayor a 0 ",
                             },
                    accion:{
                             required:"Seleccione una opción"
                             },
                 },
                 errorElement:"em",errorPlacement:function(error,element){
                        error.addClass("help-block");
                        element.parents(".form-group col-md-12").addClass("has-feedback");
                        
                        if(element.prop("type")==="checkbox"){
                         error.insertAfter(element.parent("label"));
                        }
                        else{
                         error.insertAfter(element.parent());
                        }
                        if(!element.next("span")[0])
                        {
                         $("<span class=\"fa fa-remove form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
         
                     },
                     success:function(label,element){
                         if(!$(element).next("span")[0])
                        {
                         $("<span class=\"fa fa-ok form-control-feedback \" style=\"padding-top:10px;\"></span>").insertAfter($(element));
                        }
                     },
                     highlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-error").removeClass("has-success");
                         $(element).parents(".col-md-6").addClass("has-error").removeClass("has-success");
                         $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                         
                     },
                     unhighlight:function(element,errorClass,validClass){
                         $(element).parents(".form-group col-md-12").addClass("has-success").removeClass("has-error");
                         $(element).parents(".col-md-6").addClass("has-success").removeClass("has-error");
                         $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                         },
                 });
             });
     
 </script>
 
  
</body>

</html>
