 <?php
#sesiones
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/font-awesome.min.css">
   <title>CCI</title>
<!--zona de bootstrap -->
  <!--  <link rel="stylesheet" href="../css/bootstrap.min.css">-->
   
     <link rel="stylesheet" href="../css/styleCCI.min.css">
    <!--  <link href="https://fonts.googleapis.com/css?family=Noto+Serif" rel="stylesheet"> -->
   
    <!--zona de css propietario -->
    <link rel="stylesheet" href="../js/bootstrap.min.js">
    <!--zona de font awesmoe -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

</head>
<body>
 
 <?php
       if(isset($_SESSION['token']) && $_SESSION['token']==="%$123456AaA")
        {
          header('Location: ../php/Nlogout.php');
    ?>
        <p class="help-block"> Bienvenido <?php echo $_SESSION['nombre']; ?></p> <!-- los datos que son enviados POST(menuUsuario)  -->
        
        <?php
        }//fin del if session token
        else
        {
            ?>
 <header class="main-header" role="banner">
 
 <img src="../img/admin.jpg" alt="Logo" class="responsive" class="center"  >
      </header>
          
          <!-- agregado-->
           
     </div>
  </div>


<div class="navbar">
  <a href="./Administrador.php"><span class="fa fa-home"></a>
 <!-- <div class="dropdown">
    <button class="dropbtn">Configuraciones 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="#" target="contenido">Activar/Desactivar cuentas</a>
      <a href="#" target="contenido">Número máximo de prestamos</a>
      <a href="#" target="contenido" >Carreras</a>
      <a href="#" target="contenido" >Departamentos</a>
    </div>
  </div> -->
  <div class="dropdown">
    <button class="dropbtn">Administrador 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
     <a href="RegistrarAdministrador.php" target="contenido" >Registrar Nuevo</a>
     <a href="EditarAdministrador.php" target="contenido" >Editar</a>
    </div>
  </div>
  <div class="dropdown">
    <button class="dropbtn">Encargados 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
     <a href="NuevoEncargado.php" target="contenido"  >Registrar Nuevo</a>
     <a href="EditarEncargado.php" target="contenido" >Editar</a>
    </div>
  </div>
  
  
  <div class="dropdown">
    <button class="dropbtn">Reportes 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
     <a href="ReporteTransacciones.php" target="contenido"  >Reporte de transacciones</a>
     <a href="ReporteAdeudosCreditos.php" target="contenido" >Reporte Adeudos Creditos</a>
    </div>
  </div>

 <!-- <div class="dropdown">
    <button class="dropbtn">Reportes
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="#" target="contenido" >Fechas de inicio y fin de reportes por default</a>
    </div>
  </div> -->
  
   <div class="dropdownr"> 
   <button class="dropbtn">Extras <span class="fa fa-gears"></button>
    </button>
    <div class="dropdown-content">
     <a href="./RegistrarEstudiantes.php" target="contenido">Registrar Estudiante</a>
     <a href="./RegistroTicketEncargado.php" target="contenido">Registrar Ticket</a>
     <a href="./EditarPasswordAdministrador.php" target="contenido">Cambiar Contraseña</a>
     <a href="./Configuraciones.php" target="contenido">Configuraciones del sistema</a>
     <a style="color: red" <a href="../php/Nlogout.php">Cerrar sesión</a>
    </div>
  </div> 
</div>

  <!-- Apoyo para el logueo y sesiones-->
                  <input type="hidden" name="token" value='%$123456AaA'>
      
        <h3> <!--<img src="../img/mpengu.png" width= 50px;>-->&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<?php echo ( $_SESSION['nombre']);?><a>   </a><?php echo ($_SESSION['apellidos']);?>   <i><a class="pull-right"> <?php echo  ( $_SESSION['departamento']);?> </a></i></h3>   
  

<br>
 <iframe name="contenido"  scrolling="si"></iframe>
 <?php
        }//fin else
        ?>
        
   <!--zona de js -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"> </script> 
<script language="javascript" src="../js/bootstrap.min.js"> </script>
<script language="javascript" src="../js/jquery.validate.min.js"> </script>
<script language="javascript" src="../js/additional-methods.min.js"> </script>
<script language="javascript" src="../js/messages_es.min.js"> </script>
<script language="javascript" src="../js/messages_es.js"></script>

<script language="javascript"> //script para loguearse
 
 
        $(document).ready(function(){
      
                var token=$('#token').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NseguridadToken.php',
                        dataType:'json',
                        data:'token='+token, //asociado y valor
                        success:function(resultado)
                        {
                            if(resultado.exito)
                            {
                                  //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                               var aux1 =  $('#numero_control').val();
                              // $('#numero_control').val(aux1.substring(0,2)+'540'+ (parseInt(aux1.substring(5,8)) +1));
                              $('#numero_control').val(parseInt(aux1)+1);
                            //   $('#correo').focus();
                               $('#numero_control').select();
                              alert('Usuario con ese número de control ya existe!!, sugeriremos uno diferente');
                              
                          
                            }//fin del if
                            else
                            {
                         /*      $('#numero_control').val('');
                               $('#creditos_disponibles').val('');
                               $('#impresiones_resta').val('');
                               $('#numero_control').focus();*/
                              
                              
                            }//fin del else
                        },
                        error:function(e)
                        {
                            console.log(e.responseText);
                        }
                    });//fin ajax
                
      
            
            });//fin del documenr
    </script>
</body>
</html>