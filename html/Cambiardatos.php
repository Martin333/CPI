 <?php
#sesiones
    //Inicializar la sesión
    session_start();
    if (isset($_SESSION['nombre_usuario'])) {
        //asignar a variable
        $usernameSesion = $_SESSION['nombre_usuario'];
        //asegurar que no tenga "", <, > o &
        $nombre_usuario = htmlspecialchars($usernameSesion);
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario -->
    
    <!--zona de font awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
</head>

<body>
<div class="panel-heading text-center col-md-12" style="background: navy">
<h3 class="panel-title" style="color: white">Actualizar datos</h3>
</div>
<br>
<br>
<br>
<div class=" col-md-3 pull-right">
<button class="fa fa-question-circle"  style="color: navy; height: 25px; width: 35px"  data-toggle="modal" data-target="#myModal"></button>
      </div>
<br>
<div class="panel-body col-md-12">
<form id="editarEstudiante" name="editarEstudiante" method="post" class="form-horizontal" action="" novalidate="novalidate">
<div class="form-group">
<label class="col-sm-4 control-label" for="nombreUsuario">Nombre Usuario</label>
<div class="col-sm-5">
<input class="form-control" id="nombreUsuario" name="nombreUsuario" readonly="readonly" placeholder="Nombre"  type="text" value="<?php echo $nombre_usuario; ?>">
</div>
</div>

<div class="form-group">
<label class="col-sm-4 control-label" for="firstname1">Nombre</label>
<div class="col-sm-5">
<input class="form-control" id="firstname1" name="firstname1" placeholder="Nombre" type="text">
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="lastname1">Apellidos</label>
<div class="col-sm-5">
<input class="form-control" id="lastname1" name="lastname1" placeholder="Apellido" type="text">
</div>
</div>
<!--<div class="form-group">
<label class="col-sm-4 control-label" for="username1">Username</label>
<div class="col-sm-5">
<input class="form-control" id="username1" name="username1" placeholder="Username" type="text">
</div>
</div>-->
<div class="form-group">
<label class="col-sm-4 control-label" for="email1">Email</label>
<div class="col-sm-5">
<input class="form-control" id="email1" name="email1" placeholder="Email" type="text">
</div>
</div>

<div class="form-group">
<label class="col-sm-4 control-label" for="telefono">Telefono</label>
<div class="col-sm-5">
<input class="form-control" id="telefono" name="telefono" placeholder="Telefono" type="tel">
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: navy">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Ayuda</h4>
      </div>
      <div class="modal-body">
       <p><strong>* Nombre y Apellido:</strong> Llene estos campos con sus nombre y sus apellidos.</p>
        
       <p><strong>* Email:</strong> Llene este campo con su dirrecion de correo electronico.</p>
        
        <p><strong>* Telefono:</strong> Llene este campo con los digitos de su numero telefonico.</p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
   </div>
    
<!--<div class="form-group">
<label class="col-sm-4 control-label" for="confirm_password1">Confirmar Contraseña</label>
<div class="col-sm-5">
<input class="form-control" id="confirm_password1" name="confirm_password1" placeholder="Confirmar contraseña" type="password">
</div>
</div>
<div class="form-group">
<div class="col-sm-5 col-sm-offset-4">
<div class="checkbox">
<label>
<input id="agree1" name="agree1" value="agree" type="checkbox">He leido los terminos y condiciones
</label>
</div>-->
</div>
</div>
<div class="form-group">
<div class="col-sm-9 col-sm-offset-4">
<button type="submit" class="btn btn-primary" name="signup1" value="Sign up">Aceptar</button>
</div>
</div>
</form>
</div>
        <!-- zona de JS -->
<script language="javascript" src="../js/jquery-3.3.1.min.js"></script>
<script language="javascript" src="../js/bootstrap.min.js"></script>

<!-- agregar apoyos de validación -->
<script language="javascript" src="../js/jquery.validate.min.js"></script>
<script language="javascript" src="../js/messages_es.js"></script>
<script language="javascript" src="../js/additional-methods.min.js"></script>

<script> //Script para buscar adeudos mas recientes
        $(document).ready(function(){
                var nombreU=$('#nombreUsuario').val();
                $.ajax({
                        type:'GET',
                        url:'../php/NmostrarEstudiante.php',
                        dataType:'json',
                        data:'nombreU='+nombreU, //asociado y valor
                        success:function(resultado)
                        {
                          
                            if(resultado.exito)
                            {
                               $('#firstname1').val(resultado.nombre);
                               //$('#nombre').prop("disabled", false);
                               $('#lastname1').val(resultado.apellidos);
                               $('#email1').val(resultado.correo);
                               $('#telefono').val(resultado.telefono);   
                            }//fin del if
                            else
                            {                           
                               //modificar formulario
                               $('.error').slideDown('slow');
                               setTimeout(function(){
                                    $('.error').slideUp('slow');
                               }, 3000);
                            }//fin del else
                        },
                        error:function(e)
                        {
                          
                            console.log(e.responseText);
                        }
                    });//fin ajax
            
            });//fin del documenr
    </script>

<script language="javascript">
    
		$.validator.setDefaults( {
			submitHandler: function () {
			 var datosFormulario=$("#editarEstudiante").serialize();
       //datosFormulario+="&sumado=nose";
       //console.log(datosFormulario);
       $.ajax({
         type:"POST",
         url:"../php/NeditarAlumno.php",//no autollamar
         dataType:"json",
         data:datosFormulario
         })
       .done(function(respuesta){
         if(respuesta.exito)
         {
         alert('Cambio Exitoso');
        
           $(location).attr('href','./Estudiante.php'); // equivalente a header
         }
         else
         {
          
             $('.error').slideDown('slow');
             setTimeout(function(){
                  $('.error').slideUp('slow');
             }, 3000);
             
         }
          })
        
       
       .fail(function(e){
         console.log(e.responseText);
        });
         //fin de ajax
			}
		} );
    
    

		$( document ).ready( function () {
			$("#editarEstudiante").validate( {
				rules: {
					firstname: "required",
					lastname: "required",
					username: {
						required: true,
						minlength: 2
					},
					password: {
						required: true,
						minlength: 9
					},
					confirm_password: {
						required: true,
						minlength: 8,
						equalTo: "#password"
					},
					email: {
						required: true,
						email: true
					},
					agree: "required"
				},
				messages: {
					firstname: "Ingresa tu nombre",
					lastname: "Ingresa tu apellido",
					username: {
						required: "Please enter a username",
						minlength: "Your username must consist of at least 2 characters"
					},
					telefono: {
						required: "Ingresa un numero telefonico",
						minlength: "El  numero telefonico debe contener al menos 10 numeros"
					},
					email: "Email no valido",
					agree: "Acepta nuestros terminos y condiciones"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
			} );

			$( "#editarEstudiante" ).validate( {
				rules: {
					firstname1: "required",
					lastname1: "required",
					username1: {
						required: true,
						minlength: 2
					},
					password1: {
						required: true,
						minlength: 9
					},
					confirm_password1: {
						required: true,
						minlength: 8,
						equalTo: "#password1"
					},
					email1: {
						required: true,
						email: true
					},
					agree1: "required"
				},
				messages: {
					firstname: "Ingresa tu nombre",
					lastname: "Ingresa tu apellido",
					username: {
						required: "Please enter a username",
						minlength: "Your username must consist of at least 2 characters"
					},
					password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 9 caracteres"
					},
					confirm_password: {
						required: "Ingresa una contraseña",
						minlength: "La contraseña debe contener al menos 8 caracteres",
						equalTo: "Las contraseñas deben coincidir"
					},
					email: "Email no valido",
					agree: "Acepta nuestros terminos y condiciones"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-remove form-control-feedback'style='padding-top:10px'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='fa fa-check form-control-feedback' style='padding-top:10px'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "fa-remove" ).removeClass( "fa-check" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "fa-check" ).removeClass( "fa-remove" );
				}
			} );
		} );
	
</script>
    
</body>

</html>