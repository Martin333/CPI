<!DOCTYPE html>
<html dir="en">
    <head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
      <!--zona de bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--zona de css propietario-->
    <!--zona de fuente (font awesome)-->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <script LANGUAGE="JavaScript" type="text/javascript">
    function clave(){
        Co1 = document.getElementById("Co1").value;
        Co2 = document.getElementById("Co2").value;
       Eti  = document.getElementById("econ").style;
       tacha = document.getElementById("tacha").style;
       err = document.getElementById("conal").style;
   	camV = document.getElementById("alnom").style;
      valcon = document.getElementById("alncon1").style;
      valcon2 = document.getElementById("alcon2").style;
      valnom = document.getElementById("lname").value;
      valnom = document.getElementById("fname").value;
      valnom = document.getElementById("fname").value;
      
      if(valnom == ""){
         camV.display ="";
         return false;
      }
      else{
         camV.display ="none";
         
      }
      if(Co1 == ""){
         valcon.display="";
      }
      if(Co2 == ""){
         valcon2.display="";
         return false;
      }
      else if (Co1 != Co2)
    {
      valcon.display="none";
      valcon2.display="none";
         Eti.color ="red";
         tacha.display= "";
         err.display="";
         return false;
    }
    else{
      Eti.color ="black";
         tacha.display= "none";
         err.display="none";
    }
} 
</script> 
    </head>
   <body>
      <div class="container" style="border: groove">
         <div class="form-group" style="background: navy; text-align: center; ">
               <h1 style="color:white; background: navy">Registrar nuevo encargado   <span class="text-right"><i class="fa fa-user-circle bigicon"></i></span></h1>
         </div>
         <div class="container" >
        <div class="row">
        <div class="col-md-12" >
            <div class="form-container" >
                <form class="form-horizontal" method="post" >
                    <fieldset>
                        <legend class="text-center header">Datos</legend>
                        
                           <div class="form-group">
                            <label class="col-md-2 col-md-offset-3">Nombre:</label>
                            
                            <div class="input-group col-md-4">
                                <span class="input-group-addon text-center"><i class="fa fa-user bigicon"></i></span>
                                <input id="fname" name="name" type="text" placeholder="Nombre" class="form-control" maxlength="30" >
                                
                            </div>
                           <div id="alnom" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                            
                        </div>
                        <div class="form-group">
                            
                            <label class="col-md-2 col-md-offset-3">Apelido Paterno:</label>
                            <div class="input-group col-md-4">
                                <span class="input-group-addon   text-center"><i class="fa fa-user bigicon"></i></span>
                                <input id="lname" name="name" type="text" placeholder="Apelido Paterno" class="form-control" maxlength="30">
                            </div>
                            <div id="alap1" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                        </div>
                        <div class="form-group">
                           
                            <label class="col-md-2 col-md-offset-3">Apelido Materno:</label>
                            <div class="input-group col-md-4">
                                 <span class="input-group-addon text-center"><i class="fa fa-user bigicon"></i></span>
                                <input id="lname2" name="name" type="text" placeholder="Apelido Materno" class="form-control" maxlength="30" >
                            </div>
                            <div id="alap2" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                        </div>
                        <div class="form-group">
                            
                             <label class="col-md-2 col-md-offset-3">Contraseña:</label>
                            <div class="input-group col-md-4">
                                <span class="input-group-addon text-center"><i class="fa fa-lock bigicon"></i></span>
                                <input id="Co1" name="Co1" type="password" placeholder="Contraseña" class="form-control"  maxlength="15" >
                            </div>
                            <div id="alncon1" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                        </div>
                        <div class="form-group">
                           
                            <label class="col-md-2 col-md-offset-3" id="econ" name="econ">Confirmar contraseña:</label>
                            <div class="input-group col-md-4">
                                 <span class="input-group-addon text-center"><i class="fa fa-lock bigicon"></i></span>
                                <input id="Co2" name="Co2" type="password" placeholder="Contraseña" class="form-control" maxlength="15">
                                <span class="input-group-addon text-center"  id="tacha"  style="color: red; display: none "><i class="fa fa-times-circle bigicon"></i></span>
                            </div>
                            <div id="conal" class="alert alert-danger col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Contraseña incorrecta!</strong> teclea la contraseña igual que la anterior
                            </div>
                            <div id="alcon2" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                        </div>

                        <div class="form-group">
                           
                            <label class="col-md-2 col-md-offset-3">Correo:</label>
                            <div class="input-group col-md-4">
                                 <span class="input-group-addon text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                                <input id="email" name="email" type="email" placeholder="Ejemplo@itdelicias.edu.mx" class="form-control" maxlength="40" pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@itdelicias.edu.mx" required>
                            </div>
                            <div id="alcor" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                        </div>

                        <div class="form-group">
                            
                            <label class="col-md-2 col-md-offset-3">Telefono:</label>
                            <div class="input-group col-md-4">
                                <span class="input-group-addon text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                <input id="phone" name="phone" type="text" placeholder="Telefono" class="form-control" maxlength="10">
                            </div>
                            <div id="alntel" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                        </div>
                        
                        <!-- <div class="form-group">
                            <label class="col-md-2 col-md-offset-3">No.Control:</label>
                            
                            <div class="input-group col-md-4">
                                <span class="input-group-addon text-center"><i class="fa fa-hashtag bigicon"></i></span>
                                <input id="fname" name="name" type="text" placeholder="##540###" class="form-control" maxlength="30" pattern="^[0-9][0-9]+540+[0-9][0-9][0-9]" required>
                                
                            </div>
                           <div id="alnom" class="alert alert-warning col-md-4 col-md-offset-5 panel " style="display: none">
                              <strong>¡Campo requerido!</strong> Ingresa los datos solicitados en este campo. 
                            </div>
                            
                        </div>-->
   
                        <div class="form-group">
                           
                            <label class="col-md-2 col-md-offset-3">Nota:</label>
                            <div class="input-group col-md-4">
                                 <span class="input-group-addon text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                                <textarea class="form-control" id="message" name="message" placeholder="Nota(Opcional):" rows="7"></textarea>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-11" style="text-align:right">
                                <button type="summit" class="btn btn-primary" style="background: black"  onclick="clave();">Enviar</button>
                            </div>
                        </div> <!-- -->
   
                        
                    </fieldset>
                  </form>
                </div>
             </div>
         </div>
      </div>
   </div>
    
   </body>
</html>