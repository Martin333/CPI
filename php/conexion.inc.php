<?php
//incluir configuracion y liberia de apoyo ADODB
#include()
#include_once()
#require()
#require_once()

include ("config.inc.php");
include ('../lib/adodb5/adodb.inc.php'); //punto(.) concatenacion es por eso que se usa ->

//Uso de clases abstractas para evitar la creación directa de objetos
// clase para usar en todas las acciones del sistema
//abstract para la creacion de objetos a traves de la clase, evita el uso directo

abstract class Conectar
{
    private $conn, $tipo = TIPO, $servidor = SERVIDOR, $usuario = USUARIO, $password = PASSWORD, $basedatos = BASEDATOS;
    
    #crear metodo
    public function CONECTAR() //-> sustituto de .
    {
        try
        {
          if(!$this->conn=ADONewConnection($this->tipo))
          {
            throw new Exception('Error al cargar el driver');
          }
          elseif (!$this->conn->Connect($this->servidor, $this->usuario, $this->password, $this->basedatos))
          {
            throw new Exception('Error con la conexión a la Base de Datos');
          }
       //   mysqli_query("SET NAMES 'utf8'");
          return $this->conn;

        }
        catch (Exception $e)
        {
            //intencion es mostrar los mensajes en HTML
?> <!-- por eso se cierra  -->

            <h1>A ocurrido un error: <?php echo $e->getMessage(); ?> </h1>
          <!--<h1 class="has-error">A ocurrido un Error en la conexi&oacute;n:</h1> -->
          <h2>El error en la L&iacute;nea: <?php echo $e->getLine(); ?></h2>
<?php
        }
    }//Fin de metodo CONECTAR
}//Fin de clase Conectar
?>