<?php
//evitarndo los parentesis
include 'conexion.inc.php';

class Usuarios extends Conectar
{
    private $db;
    public function __construct()
    {
        $this->db = parent::Conectar(); //Estamos heredando, hacemos referencia a la herencia
    }//Fin de la function __construct()
    
       
    //metodo logueo
     public function loginUsuario($nombre_usuario,$password)
    {
        
    $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
     $sqlUsuario="SELECT carrera,tipo_usuario,nombre_usuario,nombre,apellidos, correo, departamento,estado,creditos_actuales,adeudos,numero_control, estado_trasferencia FROM tbl_usuarios 
                WHERE (nombre_usuario = '$nombre_usuario' or numero_control = '$nombre_usuario') and password='$password' and estado='activo'";
    //  $sqlUsuario="CALL spLoginUsuario('$nombreUsuario','$password')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->fetchrow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
        // $arreglo=[];
         //$arreglo[]=$qryUsuario;
         
    
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }

}

class Encargado extends Conectar
{
        private $db;
    public function __construct()
    {
        $this->db = parent::Conectar(); //Estamos heredando, hacemos referencia a la herencia
    }//Fin de la function __construct()
    
   
    
        public function buscarNoControl($numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
     $sqlUsuario="SELECT tipo_usuario,nombre_usuario,nombre,apellidos, correo, estado FROM tbl_usuarios WHERE numero_control = '$numero_control'";
    //  $sqlUsuario="CALL spBuscarNoControl('$numero_control')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function buscarNoTicket($numero_ticket)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT  numero_control, numero_ticket FROM tbl_tickets WHERE numero_ticket = '$numero_ticket'";
      //$sqlUsuario="CALL spBuscarNoTicket('$numero_ticket')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function buscarCreditosTicket()//falta procedimiento almacenado
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT  creditos_ticket FROM tbl_configuraciones";
      //$sqlUsuario="CALL spBuscarNoTicket('$numero_ticket')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function registrarTicket($numero_control,$numero_ticket)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="insert into tbl_tickets(numero_control,fecha,numero_ticket) values('$numero_control',CURDATE(),'$numero_ticket')";
      //$sqlUsuario="CALL spRegistrarTicket('$numero_control','$numero_ticket')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    
    public function registrarCreditosActualesTicket($numero_control,$creditos_ticket)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="(SELECT  numero_creditos_actuales,nombre,apellidos FROM tbl_creditos_actuales inner join tbl_usuarios 
on tbl_creditos_actuales.numero_control = tbl_usuarios.numero_control
where tbl_creditos_actuales.numero_control = '$numero_control' 
ORDER BY tbl_creditos_actuales.id_creditos_actuales DESC LIMIT 0,1)";
      //$sqlUsuario="CALL spBuscarCreditosActualesMasReciente('$numero_control')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
      //var_dump($qryCreditosActuales['numero_creditos_actuales']);
        $creditosActuales = implode($qryCreditosActuales);
        
    //  print_r(intval($qryCreditosActuales['numero_creditos_actuales']));
     if($qryCreditosActuales and $qryCreditosActuales['numero_creditos_actuales']>=0)
     {
        
        $creditosActuales= intval($qryCreditosActuales['numero_creditos_actuales']) + intval($creditos_ticket);
        $sqlAdeudos="SELECT numero_adeudos FROM tbl_adeudos where numero_control = '$numero_control' ORDER BY id_adeudos DESC LIMIT 0,1";
        //$sqlAdeudos="CALL spBuscarAdeudosPendientes('$numero_control')";
        $qryAdeudos=$this->db->Execute($sqlAdeudos)->FetchRow();
        $numeroAdeudos= intval($qryAdeudos['numero_adeudos']);
        if($numeroAdeudos>0 )
        {            
            if($numeroAdeudos>$creditosActuales)
            {
                $numeroAdeudos= $numeroAdeudos-$creditosActuales;
                $creditosActuales=0;
            }
            
            else
            {
                if($numeroAdeudos==$creditosActuales)
                {
                     $creditosActuales=0;
                     $numeroAdeudos=0;
                    
                }
                else
                {
                    $creditosActuales=$creditosActuales-$numeroAdeudos;
                    $numeroAdeudos=0;
                }
            }
            
            $sqlUsuario="insert into tbl_creditos_actuales(numero_creditos_actuales,fecha,numero_control) values('$creditosActuales',CURDATE(),'$numero_control')";
            //$sqlUsuario="CALL spRegistrarCreditosActualesTransaccion('$numero_control','$creditosActuales')";
            $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow();
            $sqlUsuario3="update tbl_usuarios set creditos_actuales= '$creditosActuales' where numero_control='$numero_control'";
            //$sqlUsuario3="CALL spActualizarCreditosUsuario('$numero_control','$creditosActuales')";
            $qryUsuario3= $this->db->Execute($sqlUsuario3)->FetchRow();
            $sqlUsuario2="insert into tbl_adeudos(numero_adeudos,fecha,numero_control) values('$numeroAdeudos',CURDATE(),'$numero_control')";
            //$sqlUsuario2="CALL spRegistrarPrestamoImpresiones('$numero_control','$numeroAdeudos')";
            $qryUsuario2= $this->db->Execute($sqlUsuario2)->FetchRow();
            $sqlUsuario4="update tbl_usuarios set adeudos= '$numeroAdeudos' where numero_control='$numero_control'";
            //$sqlUsuario4="CALL spActualizarAdeudosUsuario('$numero_control','$numeroAdeudos')";
            $qryUsuario4= $this->db->Execute($sqlUsuario4)->FetchRow();
             $_SESSION['creditos_actuales'] = $creditosActuales;
        }
        else
        {
            $sqlUsuario="insert into tbl_creditos_actuales(numero_creditos_actuales,fecha,numero_control) values('$creditosActuales',CURDATE(),'$numero_control')";
            //$sqlUsuario="CALL spRegistrarCreditosActualesTransaccion('$numero_control','$creditosActuales')";
            $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow();
            $sqlUsuario3="update tbl_usuarios set creditos_actuales= '$creditosActuales' where numero_control='$numero_control'";
            //$sqlUsuario3="CALL spActualizarCreditosUsuario('$numero_control','$creditosActuales')";
            $qryUsuario3= $this->db->Execute($sqlUsuario3)->FetchRow();
             $_SESSION['creditos_actuales'] = $creditosActuales;
        }
        
      } 
      
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
     public function buscarCreditosActuales($numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="(SELECT  numero_creditos_actuales,nombre,apellidos  
FROM tbl_creditos_actuales inner join tbl_usuarios 
on tbl_creditos_actuales.numero_control = tbl_usuarios.numero_control
where tbl_creditos_actuales.numero_control = '$numero_control' 
ORDER BY tbl_creditos_actuales.id_creditos_actuales DESC LIMIT 0,1)";
      //$sqlUsuario="CALL spBuscarCreditosActualesMasReciente('$numero_control')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
          
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryCreditosActuales,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function registrarCreditosActualesTransaccion($numero_control,$creditosRestantes)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="insert into tbl_creditos_actuales(numero_creditos_actuales,fecha,numero_control) values('$creditosRestantes',CURDATE(),'$numero_control')";
      //$sqlUsuario="CALL spRegistrarCreditosActualesTransaccion('$numero_control','$creditosRestantes')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
      public function actualizarCreditosUsuario($numero_control,$creditosRestantes)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="update tbl_usuarios set creditos_actuales= '$creditosRestantes' where numero_control='$numero_control'";
      //$sqlUsuario="CALL spActualizarCreditosUsuario('$numero_control','$creditosRestantes')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function actualizarEstadoTrasferenciaUsuario($estadoTrasferencia, $numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="update tbl_usuarios set estado_trasferencia= '$estadoTrasferencia' where numero_control='$numero_control'";
      //$sqlUsuario="CALL spActualizarCreditosUsuario('$numero_control','$creditosRestantes')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
      public function actualizarTransaccionSolicitudAprobar($numero_control_emisor,$tipo_transaccion,$cantidad,$numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="update tbl_transacciones set numero_control= '$numero_control_emisor', tipo_transferencia='$tipo_transaccion', creditos_transaccion=$cantidad, numero_control_receptor='$numero_control  where numero_control='$numero_control' and numero_control_receptor='numero_control_emisor' and tipo_transaccion='solicitud_trasferencia'";
      //$sqlUsuario="CALL spActualizarAdeudosUsuario('$numero_control','$creditosPrestados')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
       public function actualizarAdeudosUsuario($numero_control,$creditosPrestados)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="update tbl_usuarios set adeudos= '$creditosPrestados' where numero_control='$numero_control'";
      //$sqlUsuario="CALL spActualizarAdeudosUsuario('$numero_control','$creditosPrestados')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
        public function actualizarTransaccion($numero_control,$creditosPrestados)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="update tbl_usuarios set adeudos= '$creditosPrestados' where numero_control='$numero_control'";
      //$sqlUsuario="CALL spActualizarAdeudosUsuario('$numero_control','$creditosPrestados')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
     public function actualizarTrasferenciaUsuario($numero_control,$estado_trasferencia)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="update tbl_usuarios set estado_trasferencia= '$estado_trasferencia' where numero_control='$numero_control'";
      //$sqlUsuario="CALL spActualizarTrasferenciaUsuario('$numero_control','$estado_trasferencia')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function registrarTransaccionResta($numero_control,$impresiones_resta)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="insert into tbl_transacciones(tipo_transaccion,creditos_transaccion,fecha_transaccion,numero_control,departamento_transaccion,nombre_usuario,tipo_usuario,numero_ticket,numero_control_receptor) 
values('$tipo_transaccion','$creditos_transaccion',CURDATE(),'$numero_control','$departamento','$encargado','$tipo_encargado','$numero_ticket','$numero_control_receptor')";
      //$sqlUsuario="CALL spRegistrarTransaccion('$numero_control','resta_impresiones','$impresiones_resta')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function buscarMaxPrestamos()
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT numero_maximo_prestamos FROM tbl_configuraciones";
      //$sqlUsuario="CALL spBuscarMaxPrestamos()";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function buscarCarreras()
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT nombre_carrera FROM tbl_carreras";
      //$sqlUsuario="CALL spBuscarCarreras()";
      $qryUsuario= $this->db->Execute($sqlUsuario); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
      $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $combobit;
    }
    public function listarCarreras()
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT nombre_carrera FROM tbl_carreras";
      //$sqlUsuario="CALL spBuscarCarreras()";
      $qryUsuario= $this->db->Execute($sqlUsuario); //Cuando se usan procedimientos almacenados, se usa FetchRow()
       $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
      $mensaje['usuarios']=[]; //$mensaje['usuarios'][0]
      $aUsuarios=[];
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        while(!$qryUsuario->EOF) {
            $aUsuarios[]=array('carreras'=>$qryUsuario->fields('nombre_carrera')
                             //  'fecha'=>$qryUsuario->fields('fecha_transaccion')
                               //'tipo'=>$qryUsuario->fields('tipo_U'),
                               //'foto'=>base64_encode($qryUsuario->fields('foto_U'))
                               );
            $qryUsuario->MoveNext();
        }
        $mensaje['usuarios']=$aUsuarios;
      }
      return $mensaje;
    }
    public function listarUsuario($usuario,$tipo_transaccion)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT creditos_transaccion, fecha_transaccion FROM tbl_transacciones WHERE numero_control='$usuario' AND tipo_transaccion='$tipo_transaccion'";
      //$sqlUsuario="CALL spReporteGastos('$usuario','$tipo_transaccion')";
      $qryUsuario=$this->db->Execute($sqlUsuario);//->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
      $mensaje['usuarios']=[]; //$mensaje['usuarios'][0]
      $aUsuarios=[];
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        while(!$qryUsuario->EOF) {
            $aUsuarios[]=array('creditos'=>$qryUsuario->fields('creditos_transaccion'),
                               'fecha'=>$qryUsuario->fields('fecha_transaccion')
                               //'tipo'=>$qryUsuario->fields('tipo_U'),
                               //'foto'=>base64_encode($qryUsuario->fields('foto_U'))
                               );
            $qryUsuario->MoveNext();
        }
        $mensaje['usuarios']=$aUsuarios;
      }
      return $mensaje;
    }//Fin de metodo buscarUsu
     public function buscarAdeudosPendientes($numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT numero_adeudos FROM tbl_adeudos where numero_control = '$numero_control' ORDER BY id_adeudos DESC LIMIT 0,1";
      //$sqlUsuario="CALL spBuscarAdeudosPendientes('$numero_control')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
          
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryCreditosActuales,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function registrarPrestamoImpresiones($numero_control,$creditosPrestados)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    //    intval($creditosRestantes);
      $sqlUsuario="insert into tbl_adeudos(numero_adeudos,fecha,numero_control) values('$creditosPrestados',CURDATE(),'$numero_control')";
      //$sqlUsuario="CALL spRegistrarPrestamoImpresiones('$numero_control','$creditosPrestados')";
      $qryCreditosActuales=$this->db->Execute($sqlUsuario)->FetchRow();
   
     
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryCreditosActuales)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function registrarTransaccion($numero_control,$tipo_transaccion,$creditos_transaccion,$departamento,$encargado,$tipo_encargado,$numero_ticket,$numero_control_receptor)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="insert into tbl_transacciones(tipo_transaccion,creditos_transaccion,fecha_transaccion,numero_control,departamento_transaccion,nombre_usuario,tipo_usuario,numero_ticket,numero_control_receptor) 
values('$tipo_transaccion','$creditos_transaccion',CURDATE(),'$numero_control','$departamento','$encargado','$tipo_encargado','$numero_ticket','$numero_control_receptor')";
      //$sqlUsuario="CALL spRegistrarTransaccion('$numero_control','$tipo_transaccion','$creditos_transaccion','$departamento','$encargado','$tipo_encargado','$numero_ticket','$numero_control_receptor')";
      $qryUsuario= $this->db->Execute($sqlUsuario); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
     //   $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    
     public function buscarEstudiante($numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="Select * from tbl_usuarios where numero_control='$numero_control'";
      //$sqlUsuario="CALL spBuscarEstudiante('$numero_control')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
          public function buscarCreditosSolicitud($numero_control)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="select creditos_transaccion from tbl_transacciones where numero_control_receptor='$numero_control' and tipo_transaccion='solicitud_trasferencia'";
      //$sqlUsuario="CALL spBuscarCreditosSolicitud('$numero_control')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
      public function buscarEstadoTrasferenciaTransaccion($numero_control_emisor)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="select tipo_transaccion from tbl_transacciones where numero_control_receptor='$numero_control_emisor' and tipo_transaccion ='solicitud_trasferencia'";
      //$sqlUsuario="CALL spBuscarCreditosSolicitud('$numero_control')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function buscarEstudianteUsuario($nombre_usuario)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="Select * from tbl_usuarios where nombre_usuario='$nombre_usuario'";
      //$sqlUsuario="CALL spBuscarEstudianteUsuario('$nombre_usuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function registrarEstudiante($numero_control,$nombre_usuario,$nombre,$apellidos, $carrera, $correo,$telefono,$contraseña)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="insert into tbl_usuarios(numero_control,nombre_usuario,nombre,apellidos,carrera, correo,telefono,password,estado, tipo_usuario,fecha_registro, creditos_actuales,adeudos)
      values('$numero_control','$nombre_usuario','$nombre','$apellidos', '$carrera', '$correo','$telefono','$contraseña','activo','estudiante',CURDATE(),0,0)";
      //$sqlUsuario="CALL spRegistrarEstudiante('$numero_control','$nombre_usuario','$nombre','$apellido', '$carrera', '$correo','$telefono','$contraseña','activo','estudiante')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    
    public function listarAdeudos()
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="select nombre,apellidos,numero_control,adeudos from tbl_usuarios where tipo_usuario='estudiante' and adeudos>0";
      //$sqlUsuario="CALL spReporteAdeudos()";
      $qryUsuario=$this->db->Execute($sqlUsuario);//->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow() en consultas multiples se quita
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
      $mensaje['adeudos']=[];//$mensaje['usuarios'][0]
      $aUsuarios=[];
          
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        while (!$qryUsuario->EOF){
            $aUsuarios[]=array(
                               'numero_control'=>$qryUsuario->fields('numero_control'),
                                'nombre'=>$qryUsuario->fields('nombre'),
                                'apellidos'=>$qryUsuario->fields('apellidos'),
                                'adeudos'=>$qryUsuario->fields('adeudos')
                               
                                                                 );
            $qryUsuario->MoveNext();
        }
        $mensaje['usuarios']= $aUsuarios;
        //$qryUsuario['foto_U']=base64_encode($qryUsuario['foto_U']);
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }//Fin de metodo buscarUsuario

    
}
class Administrador extends Conectar
{
        private $db;
    public function __construct()
    {
        $this->db = parent::Conectar(); //Estamos heredando, hacemos referencia a la herencia
    }//Fin de la function __construct()
    public function registrarAdministrador($nombreUsuario,$nombre,$apellidos,$departamento,$email,$password,$telefono)
    {
        $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="insert into tbl_usuarios(nombre_usuario,nombre,apellidos,departamento,correo,password,telefono,tipo_usuario,estado,fecha_registro)
values('$nombreUsuario','$nombre','$apellidos','$departamento','$email','$password','$telefono','admin','activo',CURDATE())";
      //$sqlUsuario="CALL spRegistrarAdministrador('$nombreUsuario','$nombre','$apellidos','$departamento','$email','$password','$telefono','admin','activo')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito'] = false;
      
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro

    }
    public function editarAdministrador($nombre,$apellidos,$departamento,$email,$telefono,$nombreUsuario)
    {
        $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="update tbl_usuarios set nombre= '$nombre', apellidos='$apellidos',departamento='$departamento',correo='$email',telefono='$telefono' where nombre_usuario='$nombreUsuario'";
      //$sqlUsuario="CALL spActualizarAdministrador('$nombre','$apellidos','$departamento','$email','$telefono','$nombreUsuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito'] = false;
      
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro

    }
    public function buscarAdministradorUsuario($nombre_u)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="select * from tbl_usuarios where nombre_usuario='$nombre_u'";
      //$sqlUsuario="CALL spBuscarAdministradorUsuario('$nombre_u')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function buscarEditarAdministradorUsuario($nombre_u)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="select * from tbl_usuarios where nombre_usuario='$nombre_u' and tipo_usuario='admin'";
      //$sqlUsuario="CALL spBuscarEditarAdministradorUsuario('$nombre_u')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    
    public function editarPasswordAdministrador($password,$nombreUsuario)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="update tbl_usuarios set password= '$password' where nombre_usuario='$nombreUsuario'";
      //$sqlUsuario="CALL spEditarPasswordAdministrador('$password','$nombreUsuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    //buscar datos del encargado
       public function buscarDatosEncargado($nombre_usuario)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="select nombre,apellidos,correo,telefono,departamento from tbl_usuarios where nombre_usuario='$nombre_usuario'";
      //$sqlUsuario="CALL spBuscarDatosEncargado('$nombre_usuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function editarEncargado($firstname1,$lastname1,$email1,$Telefono,$departamento,$nombre_usuario)
    {
        $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="update tbl_usuarios set nombre='$firstname1',apellidos='$lastname1',correo='$email1',departamento='$departamento',telefono='$Telefono' where nombre_usuario ='$nombre_usuario'";
      //$sqlUsuario="CALL spEditarEncargado('$firstname1','$lastname1','$email1','$Telefono','$departamento','$nombre_usuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito'] = false;
      
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro

    }
    
    
    public function ActulizaP($max){
         $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $maximo = (int)$max;
      $sqlUsuario="UPDATE tbl_configuraciones SET numero_maximo_prestamos = $max";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      //$mensaje=[]; //inicializar arreglo
    
      
       // $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
  
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
     
   
    }
     public function ActulizaCp($max){
         $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $maximo = (int)$max;
      $sqlUsuario="UPDATE tbl_configuraciones SET creditos_ticket = $max";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
     // $mensaje=[]; //inicializar arreglo
    
      
      //  $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
  
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
     
   
    }
     public function AgregaDepa($max){
         $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="insert into tbl_departamentos (nombre_departamento) values ('$max')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); 
    }
     public function EliminaDepa($max){
         $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="DELETE FROM tbl_departamentos WHERE tbl_departamentos.nombre_departamento = '$max'";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); 
    }
    public function AgregaCarrera($max){
         $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="insert into tbl_carreras (nombre_carrera) values ('$max')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); 
    }
    public function EliminaCarrera($max){
         $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      $sqlUsuario="DELETE FROM tbl_carreras WHERE tbl_carreras.nombre_carrera = '$max'";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); 
    }

    
    
     public function buscarUsuario($nombre_u,$estado)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    // $sqlUsuario="SELECT nombre_usuario FROM tbl_usuarios WHERE id_usuario='$idUsuario' AND contraseña='$password'";
      $sqlUsuario="UPDATE tbl_usuarios SET estado = '$estado' where numero_control = '$nombre_u'"  ;
      $qryUsuario= $this->db->Execute($sqlUsuario);//->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      //$mensaje['exito']=false;
     
     // if($qryUsuario)
      //{
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
      //  $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      //}
      return $mensaje;
    }
     public function BuscarControl($nombre_u)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    // $sqlUsuario="SELECT nombre_usuario FROM tbl_usuarios WHERE id_usuario='$idUsuario' AND contraseña='$password'";
      $sqlUsuario="select encargado from tbl_usuarios where numero_control = '$nombre_u'";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      //$mensaje['exito']=false;
     
      
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      
      return $mensaje;
    }
}
class Estudiante extends Conectar
{
    
        private $db;
    public function __construct()
    {
        $this->db = parent::Conectar(); //Estamos heredando, hacemos referencia a la herencia
    }//Fin de la function __construct()
    
    /*public function mostrarEstudiante($nombre_usuario)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
    // $sqlUsuario="SELECT nombre_usuario FROM tbl_usuarios WHERE id_usuario='$idUsuario' AND contraseña='$password'";
      $sqlUsuario="CALL spBuscarNombreUsuarioEstudiante('$nombre_usuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }*/
  /*  public function editarEstudiante($firstname1,$lastname1,$email1,$telefono,$nombreUsuario)
    {
        $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
      
      $sqlUsuario="CALL spEditarEstudiante('$firstname1','$lastname1','$email1','$telefono','$nombreUsuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito'] = false;
      
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro

    }*/
    
    public function buscarPasswordEstudiante($nombre_usuario,$password)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="Select * from tbl_usuarios where nombre_usuario='$nombre_usuario' AND password='$password'";
      //$sqlUsuario="CALL spBuscarPasswordEstudiante('$nombre_usuario','$password')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    
    public function listarTransacciones()
    {
        $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT tipo_transaccion,creditos_transaccion,fecha_transaccion,numero_control,departamento_transaccion  FROM tbl_transacciones";
      //$sqlUsuario="CALL spBuscarCarreras()";
      $qryUsuario= $this->db->Execute($sqlUsuario); //Cuando se usan procedimientos almacenados, se usa FetchRow()
       $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
      $mensaje['trans']=[]; //$mensaje['usuarios'][0]
      $aUsuarios=[];
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        while(!$qryUsuario->EOF) {
            $aUsuarios[]=array('tipo'=>$qryUsuario->fields('tipo_transaccion'),
                               'creditos'=>$qryUsuario->fields('creditos_transaccion'),
                               'fecha'=>$qryUsuario->fields('fecha_transaccion'),
                               'numero'=>$qryUsuario->fields('numero_control'),
                               'departamento'=>$qryUsuario->fields('departamento_transaccion'),
                               );
                               
            $qryUsuario->MoveNext();
        }
        $mensaje['trans']=$aUsuarios;
      }
      return $mensaje;
    }
    
    
    
    public function editarPasswordEstudiante($password,$nombre_usuario)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="update tbl_usuarios set password= '$password' where nombre_usuario='$nombre_usuario'";
      //$sqlUsuario="CALL spEditarPasswordEstudiante('$password','$nombre_usuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario);//->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      //$mensaje=[]; //inicializar arreglo
      $mensaje['exito']=true;
     
      /*if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }*/
      return $mensaje;
    }
     public function listarUsuario($usuario,$tipo_transaccion)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT creditos_transaccion, fecha_transaccion FROM tbl_transacciones WHERE numero_control='$usuario' AND tipo_transaccion='$tipo_transaccion'";
      //$sqlUsuario="CALL spReporteGastos('$usuario','$tipo_transaccion')";
      $qryUsuario=$this->db->Execute($sqlUsuario);//->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
      $mensaje['usuarios']=[]; //$mensaje['usuarios'][0]
      $aUsuarios=[];
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        while(!$qryUsuario->EOF) {
            $aUsuarios[]=array('creditos'=>$qryUsuario->fields('creditos_transaccion'),
                               'fecha'=>$qryUsuario->fields('fecha_transaccion')
                               //'tipo'=>$qryUsuario->fields('tipo_U'),
                               //'foto'=>base64_encode($qryUsuario->fields('foto_U'))
                               );
            $qryUsuario->MoveNext();
        }
        $mensaje['usuarios']=$aUsuarios;
      }
      return $mensaje;
    }//Fin de metodo buscarUsu
    public function listarUsuario2($usuario,$tipo_transaccion)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="SELECT creditos_transaccion, fecha_transaccion, numero_control, numero_control_receptor, 
CASE WHEN numero_control='$usuario'
THEN 'emisor'
ELSE 'receptor'
END AS estado
FROM tbl_transacciones WHERE tipo_transaccion='$tipo_transaccion' AND (numero_control='$usuario' OR numero_control_receptor='$usuario')";
      //$sqlUsuario="CALL spReporteTransferencias('$usuario','$tipo_transaccion')";
      $qryUsuario=$this->db->Execute($sqlUsuario);//->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
      $mensaje['usuarios']=[]; //$mensaje['usuarios'][0]
      $aUsuarios=[];
      if($qryUsuario)
      {
     //  echo var_dump($qryUsuario);
        $mensaje['exito'] = true;
        while(!$qryUsuario->EOF) {
          
            
            $aUsuarios[]=array('creditos'=>$qryUsuario->fields('creditos_transaccion'),
                               'fecha'=>$qryUsuario->fields('fecha_transaccion'),
                               'estado'=>$qryUsuario->fields('estado')
                               
                               );
            $qryUsuario->MoveNext();

        }
       
     //  $mensaje['usuarios'] =  array_merge($qryUsuario);
       $mensaje['usuarios'] = $aUsuarios;
      }
      return $mensaje;
       
    }//Fin de metodo
    
    public function mostrarEstudiante($nombre_usuario)
    {
      $this->db->SetFetchMode(ADODB_FETCH_ASSOC);//es el metodo para cambiar la propiedad de tipo asociativa
      $sqlUsuario="Select * from tbl_usuarios where nombre_usuario='$nombre_usuario'";
      //$sqlUsuario="CALL spBuscarNombreUsuarioEstudiante('$nombre_usuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito']=false;
     
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
    }
    public function editarEstudiante($firstname1,$lastname1,$email1,$telefono,$nombreUsuario)
    {
        $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
        $sqlUsuario="update tbl_usuarios set nombre= '$firstname1', apellidos='$lastname1',correo='$email1',telefono='$telefono'
where nombre_usuario='$nombreUsuario'";
      //$sqlUsuario="CALL spEditarEstudiante('$firstname1','$lastname1','$email1','$telefono','$nombreUsuario')";
      $qryUsuario= $this->db->Execute($sqlUsuario)->FetchRow(); //Cuando se usan procedimientos almacenados, se usa FetchRow()
      $mensaje=[]; //inicializar arreglo
      $mensaje['exito'] = false;
      
      if($qryUsuario)
      {
        $mensaje['exito'] = true;
        //$qryUsuario['nombre']=htmlspecialchars($qryUsuario['nombre']);
        $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro
      }
      return $mensaje;
       // $qryUsuario['nombre']=$qryUsuario['nombre_usuario'];
       // $mensaje = array_merge($qryUsuario,$mensaje);//array_merge() es útil para integrar un arreglo en otro

    }

}
?>

